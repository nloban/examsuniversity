﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Telerik.OpenAccess;



namespace ArtushevskayaUniversityExamsDataAccess.Repositories
{
    public class Repository<T> where T : class 
    {
        protected EntitiesModel Context = EntitiesModel.Current;

        public virtual List<T> GetAll()
        {
            return this.Context.GetAll<T>().ToList();
        }

        public virtual List<T> GetBy(Expression<Func<T, bool>> predicate)
        {
            return Context.GetAll<T>().Where(predicate).ToList();
        }

        public virtual T Get(object id)
        {
            ObjectKey objectKey = new ObjectKey(typeof(T).Name, id);
            T entity = this.Context.GetObjectByKey(objectKey) as T;

            return entity;
        }

        public virtual void Add(T order)
        {
            this.Context.Add(order);
            SaveChanges();
        }

        public virtual void Remove(T order)
        {
            this.Context.Delete(order);
            SaveChanges();
        }

        public virtual void SaveChanges()
        {
            this.Context.SaveChanges();
        }

        public void Dispose()
        {
            this.Context = null;
        }

    }
}

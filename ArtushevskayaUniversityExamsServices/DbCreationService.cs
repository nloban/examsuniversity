﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsDataAccess.Repositories;

namespace ArtushevskayaUniversityExamsServices
{
    public class DbCreationService
    {
        private readonly EntitiesModel entitiesModel = EntitiesModel.Current; 
        private readonly AcademicPositionRepository academicPositionRepository = new AcademicPositionRepository();
        private readonly CathedraRepository cathedraRepository = new CathedraRepository();
        private readonly FacultyRepository facultyRepository = new FacultyRepository();
        private readonly GroupOfStudentRepository groupOfStudentRepository = new GroupOfStudentRepository();
        private readonly ScientificDegreeRepository scientificDegreeRepository = new ScientificDegreeRepository();
        private readonly SpecialityRepository specialityRepository = new SpecialityRepository();
        private readonly StudentRepository studentRepository = new StudentRepository();
        private readonly SubjectRepository subjectRepository = new SubjectRepository();
        private readonly TeacherRepository teacherRepository = new TeacherRepository();
        public DbCreationService()
        {
            CreateDatabase();
        }

        private void CreateDatabase()
        {
            if (!entitiesModel.GetSchemaHandler().DatabaseExists())
            {
                entitiesModel.GetSchemaHandler().CreateDatabase();

                string theFilePath = String.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory, "ArtushevskayaUniversityExamsDBConnection.rlinq.sql");

                using (var streamReader = new StreamReader(theFilePath))
                {
                    entitiesModel.GetSchemaHandler().ExecuteDDLScript(streamReader.ReadToEnd());
                }

                GenerateDefaultData();
            }           
        }

        private void GenerateDefaultData()
        {
            GenerateDefaultFaculties();
            GenerateDefaultCathedras();
            GenerateDefaultScientificDegrees();
            GenerateDefaultAcademicPositions();
            GenerateDefaultSpecialities();
            GenerateDefaultSubjects();
            GenerateDefaultGroupOfStudents();
            GenerateDefaultStudents();
            GenerateDefaultTeachers();
        }

        private void GenerateDefaultFaculties()
        {
            Faculty[] faculties = new[]
            {
                new Faculty {FacultyID = "ИЭФ", NameFaculty = "Инженерно-экономический факультет"},
                new Faculty {FacultyID = "ФИТУ", NameFaculty = "Факультет информационных технологий и управления "},
                new Faculty {FacultyID = "ФРЭ", NameFaculty = "Факультет радиотехники и электроники "},
                new Faculty {FacultyID = "ВФ", NameFaculty = "Венный факультет"},
                new Faculty {FacultyID = "ФКСИС", NameFaculty = "Факультет компьютерных систем и сетей"},
                new Faculty {FacultyID = "ФКП", NameFaculty = "Факультет компьютерного проектирования"},
                new Faculty {FacultyID = "ФТК", NameFaculty = "Факультет телекоммуникаций"},
                new Faculty {FacultyID = "ФЗО", NameFaculty = "Факультет заочного обучения"}
            };
            foreach (var faculty in faculties)
            {
                facultyRepository.Add(faculty);
            }
        }

        private void GenerateDefaultCathedras()
        {
            Cathedra[] cathedras = new[]
            {
                new Cathedra {CathedraID = "ЭИ", NameCathedra = "Экономической информатики"},
                new Cathedra {CathedraID = "Экономики", NameCathedra = "Экономики"},
                new Cathedra {CathedraID = "Менеджмента", NameCathedra = "Менеджмента"},
                new Cathedra {CathedraID = "Физики", NameCathedra = "Физики"},
                new Cathedra {CathedraID = "Высшей математики", NameCathedra = "Высшей математики"},
                new Cathedra {CathedraID = "ЗИ", NameCathedra = "Защиты информации"},
                new Cathedra {CathedraID = "ИГ", NameCathedra = "Инженерной графики"},
                new Cathedra {CathedraID = "Иностранных языков N1", NameCathedra = "Иностранных языков N1"},
                new Cathedra {CathedraID = "Иностранных языков N2", NameCathedra = "Иностранных языков N2"},
                new Cathedra {CathedraID = "ИИТ", NameCathedra = "Интеллектуальных информационных технологий"},
                new Cathedra {CathedraID = "Информатики", NameCathedra = "Информатики"},
                new Cathedra {CathedraID = "Метрологии и стандартизации", NameCathedra = "Метрологии и стандартизации"},
                new Cathedra {CathedraID = "ПОИТ", NameCathedra = "Программного обеспечения информационных технологий"},
                new Cathedra {CathedraID = "СУ", NameCathedra = "Систем управления"},
                new Cathedra {CathedraID = "ТОЭ", NameCathedra = "Теоретических основ электротехники"},
                new Cathedra {CathedraID = "Физвоспитания", NameCathedra = "Физвоспитания"},
                new Cathedra {CathedraID = "Философии", NameCathedra = "Философии"},
                new Cathedra {CathedraID = "Химии", NameCathedra = "Химии"},
                new Cathedra {CathedraID = "Экологии", NameCathedra = "Экологии"},
                new Cathedra {CathedraID = "Электроники", NameCathedra = "Электроники"},
                new Cathedra {CathedraID = "ЭВМ", NameCathedra = "Электронных вычислительных машин"},
                new Cathedra {CathedraID = "ЭВС", NameCathedra = "Электронных вычислительных средств"},
                new Cathedra {CathedraID = "ЭТиТ", NameCathedra = "Электронной техники и технологии"},
                new Cathedra {CathedraID = "Микро- и наноэлектроники", NameCathedra = "Микро- и наноэлектроники"},
                new Cathedra {CathedraID = "ТиОП", NameCathedra = "Тактической и общевоенной подготовки "}
            };

            foreach (var cathedra in cathedras)
            {
                cathedraRepository.Add(cathedra);
            }
        }

        private void GenerateDefaultScientificDegrees()
        {
            ScientificDegree[] scientificDegrees = new[]
            {
                new ScientificDegree {ScientificDegreeID = "Доктор наук"},
                new ScientificDegree {ScientificDegreeID = "Кандидат наук"},
                new ScientificDegree {ScientificDegreeID = "Бакалавр"},
                new ScientificDegree {ScientificDegreeID = "Магистр"}
            };

            foreach (var scientificDegree in scientificDegrees)
            {
                scientificDegreeRepository.Add(scientificDegree);
            }
        }

        private void GenerateDefaultAcademicPositions()
        {
            AcademicPosition[] academicPositions = new[]
            {
                new AcademicPosition {AcademicPositionID = "Профессор"},
                new AcademicPosition {AcademicPositionID = "Доцент"},
                new AcademicPosition {AcademicPositionID = "Аспирант"},
                new AcademicPosition {AcademicPositionID = "Ведущий научный сотрудник"},
                new AcademicPosition {AcademicPositionID = "Главный научный сотрудник"},
                new AcademicPosition {AcademicPositionID = "Младший научный сотрудник"},
                new AcademicPosition {AcademicPositionID = "Научный сотрудник"},
                new AcademicPosition {AcademicPositionID = "Старший преподаватель"},
                new AcademicPosition {AcademicPositionID = "Стажер"},
                new AcademicPosition {AcademicPositionID = "Старший научный сотрудник"},

            };

            foreach (var academicPosition in academicPositions)
            {
                academicPositionRepository.Add(academicPosition);
            }
        }

        private void GenerateDefaultSpecialities()
        {
            Speciality[] specialities = new[]
            {
                new Speciality {FacultyID = "ИЭФ", NameSpecialty = "Информационные системы и технологии в экономике", SpecialtyID = "ИСиТвЭк"},
                new Speciality {FacultyID = "ИЭФ", NameSpecialty = "Информационные системы и технологии в логистике", SpecialtyID = "ИСиТвЛог"},
                new Speciality {FacultyID = "ИЭФ", NameSpecialty = "Экономика электронного бизнеса ", SpecialtyID = "Экономика электронного бизнеса"},
                new Speciality {FacultyID = "ИЭФ", NameSpecialty = "Электронный маркетинг", SpecialtyID = "Электронный маркетинг"},
                new Speciality {FacultyID = "ФИТУ", NameSpecialty = "Информационные технологии и управление в технических системах", SpecialtyID = "ИТиУвТС"},
                new Speciality {FacultyID = "ФИТУ", NameSpecialty = "Автоматизированные системы обработки информации", SpecialtyID = "АСОИ"},
                new Speciality {FacultyID = "ФИТУ", NameSpecialty = "Искусственный интеллект", SpecialtyID = "ИИ"},
                new Speciality {FacultyID = "ФИТУ", NameSpecialty = "Промышленная электроника", SpecialtyID = "ПЭ"},
                new Speciality {FacultyID = "ФРЭ", NameSpecialty = "Радиоэлектронные системы", SpecialtyID = "РЭС"},
                new Speciality {FacultyID = "ФРЭ", NameSpecialty = "Квантовые информационные системы", SpecialtyID = "КИС"},
                new Speciality {FacultyID = "ФРЭ", NameSpecialty = "Нанотехнологии и наноматериалы в электронике", SpecialtyID = "ННЭ"},
                new Speciality {FacultyID = "ФКСиС", NameSpecialty = "Вычислительные машины, системы и сети", SpecialtyID = "ВМСиС"},
                new Speciality {FacultyID = "ФКСиС", NameSpecialty = "Информатика и технологии программирования", SpecialtyID = "ИиТП"},
                new Speciality {FacultyID = "ФКСиС", NameSpecialty = "Программное обеспечение информационных технологий", SpecialtyID = "ПОиТ"},
                new Speciality {FacultyID = "ФТК", NameSpecialty = "Защита информации в телекоммуникациях", SpecialtyID = "ЗИвТ"},
                new Speciality {FacultyID = "ФТК", NameSpecialty = "Инфокоммуникационные технологии (системы телекоммуникаций)", SpecialtyID = "ИТ"},
                new Speciality {FacultyID = "ФЗО", NameSpecialty = "Информационные системы и технологии в экономике", SpecialtyID = "ИСиТвЭк"}
            };

            foreach (var speciality in specialities)
            {
                specialityRepository.Add(speciality);
            }
        }

        private void GenerateDefaultSubjects()
        {
            Subject[] subjects = new []
            {
                new Subject {NameSubject = "Планирование на предприятии", SubjectID = "ПП"},
                new Subject {NameSubject = "Информационные технологии", SubjectID = "ИТ"},
                new Subject {NameSubject = "Компьютерная графика", SubjectID = "КГ "},
                new Subject {NameSubject = "Криптография и охрана комерческой информации", SubjectID = "КиОКИ"},
                new Subject {NameSubject = "Основы дискретной математики и теории алгоритмов", SubjectID = "ОДМиТА"},
                new Subject {NameSubject = "Основы информационных технологий", SubjectID = "ОИТ"},
                new Subject {NameSubject = "Операционные системы и системное программирование", SubjectID = "ОСиСП"},
                new Subject {NameSubject = "Основы экологии, энергосбережения и экономика природопользования", SubjectID = "ОЭЭиЭП"},
                new Subject {NameSubject = "Проектирование баз знаний", SubjectID = "ПБЗ"},
                new Subject {NameSubject = "Программное обеспечение встроенных систем", SubjectID = "ПОВС"},
                new Subject {NameSubject = "Политология", SubjectID = "Политология"},
                new Subject {NameSubject = "Полупроводниковые приборы и элементы интегральных микросхем", SubjectID = "ПППиЭИМС"},
                new Subject {NameSubject = "Периферийные устройства", SubjectID = "ПУ"},
                new Subject {NameSubject = "Системный анализ и машинное моделирование", SubjectID = "САиММ"},
                new Subject {NameSubject = "Средства и методы обеспечения информационной безопасности ", SubjectID = "СиМОИБ"},
                new Subject {NameSubject = "Стандартизация и сертификация ПО", SubjectID = " СиС ПО"},
                new Subject {NameSubject = "Сетевые информационные технологии", SubjectID = "СИТ"},
                new Subject {NameSubject = "Теория вероятностей и математическая статистика", SubjectID = "ТВиМС"},
                new Subject {NameSubject = "Эконометрика", SubjectID = "Эконометрика"},
                new Subject {NameSubject = "Электронные приборы ", SubjectID = "ЭП"},
                new Subject {NameSubject = "Экономико-математические модели и методы", SubjectID = "ЭММиМ"},
                new Subject {NameSubject = "Распределенные информационные системы", SubjectID = "РИС"}
            };

            foreach (var subject in subjects)
            {
                subjectRepository.Add(subject);
            }
        }

        private void GenerateDefaultGroupOfStudents()
        {
            GroupOfStudents[] groupOfStudents = new[]
            {
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072301", SpecialtyID = "ИСиТвЭк"},
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072302", SpecialtyID = "ИСиТвЭк"},
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072303", SpecialtyID = "ИСиТвЭк"},
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072201", SpecialtyID = "Электронный маркетинг"},
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072202", SpecialtyID = "Электронный маркетинг"},
                new GroupOfStudents {FacultyID = "ИЭФ", NumberGroupID = "072203", SpecialtyID = "Электронный маркетинг"},
                new GroupOfStudents {FacultyID = "ФКСиС", NumberGroupID = "051501", SpecialtyID = "ВМСиС"},
                new GroupOfStudents {FacultyID = "ФКСиС", NumberGroupID = "051502", SpecialtyID = "ВМСиС"},
                new GroupOfStudents {FacultyID = "ФКСиС", NumberGroupID = "051503", SpecialtyID = "ВМСиС"},
                new GroupOfStudents {FacultyID = "ФКСиС", NumberGroupID = "051504", SpecialtyID = "ВМСиС"},
                new GroupOfStudents {FacultyID = "ФКСиС", NumberGroupID = "051505", SpecialtyID = "ВМСиС"},
                new GroupOfStudents {FacultyID = "ФИТУ", NumberGroupID = "021301", SpecialtyID = "АСОИ"},
                new GroupOfStudents {FacultyID = "ФИТУ", NumberGroupID = "021302", SpecialtyID = "АСОИ"},
                new GroupOfStudents {FacultyID = "ФИТУ", NumberGroupID = "021303", SpecialtyID = "АСОИ"},
            };
            foreach (var group in groupOfStudents)
            {
                groupOfStudentRepository.Add(group);
            }
        }

        private void GenerateDefaultStudents()
        {
            Student[] students = new[]
            {
                new Student
                {
                    FacultyID = "ИЭФ",
                    NameStudent = "Иванов Иван Иванович",
                    NumberGroupID = "072302",
                    SexStudent = "Мужской",
                    SpecialtyID = "ИСиТвЭк"
                },
                new Student
                {
                    FacultyID = "ИЭФ",
                    NameStudent = "Иванова Мария Ивановна",
                    NumberGroupID = "072202",
                    SexStudent = "Женский",
                    SpecialtyID = "Электронный маркетинг"
                },
                new Student
                {
                    FacultyID = "ФКСиС",
                    NameStudent = "Иванов Иван Иванович",
                    NumberGroupID = "051502",
                    SexStudent = "Мужской",
                    SpecialtyID = "ВМСиС"
                },
                new Student
                {
                    FacultyID = "ФКСиС",
                    NameStudent = "Иванова Мария Ивановна",
                    NumberGroupID = "051502",
                    SexStudent = "Женский",
                    SpecialtyID = "ВМСиС"
                },
                new Student
                {
                    FacultyID = "ФИТУ",
                    NameStudent = "Иванов Петр Иванович",
                    NumberGroupID = "021302",
                    SexStudent = "Мужской",
                    SpecialtyID = "АСОИ"
                },
                new Student
                {
                    FacultyID = "ФИТУ",
                    NameStudent = "Иванова Екатерина Ивановна",
                    NumberGroupID = "021302",
                    SexStudent = "Женский",
                    SpecialtyID = "АСОИ"
                },
            };

            foreach (var student in students)
            {
                studentRepository.Add(student);
            }
        }

        private void GenerateDefaultTeachers()
        {
            Teacher[] teachers = new[]
            {
                new Teacher
                {
                    AcademicPositionID = "Доцент",
                    CathedraID = "ЭИ",
                    NameTeacher = "Комличенко Виталий Николаевич",
                    ScientificDegreeID = "Кандидат наук"
                },
                new Teacher
                {
                    AcademicPositionID = "Доцент",
                    CathedraID = "ЭИ",
                    NameTeacher = "Алехина Алина Энодиевна",
                    ScientificDegreeID = "Кандидат наук"
                },
                new Teacher
                {
                    AcademicPositionID = "Доцент",
                    CathedraID = "ЭИ",
                    NameTeacher = "Поттосина Светлана Анатольевна",
                    ScientificDegreeID = "Кандидат наук"
                },
            };

            foreach (var teacher in teachers)
            {
                teacherRepository.Add(teacher);
            }
        }
    }
}

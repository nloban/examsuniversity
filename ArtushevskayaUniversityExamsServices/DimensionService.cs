﻿using System;
using System.Collections.Generic;
using System.Linq;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsDataAccess.Repositories;

namespace ArtushevskayaUniversityExamsServices
{
    public class DimensionService
    {
        private readonly AcademicPositionRepository academicPositionRepository = new AcademicPositionRepository();
        private readonly CathedraRepository cathedraRepository = new CathedraRepository();
        private readonly FacultyRepository facultyRepository = new FacultyRepository();
        private readonly GroupOfStudentRepository groupOfStudentRepository = new GroupOfStudentRepository();
        private readonly ScientificDegreeRepository scientificDegreeRepository = new ScientificDegreeRepository();
        private readonly SpecialityRepository specialityRepository = new SpecialityRepository();
        private readonly StudentRepository studentRepository = new StudentRepository();
        private readonly SubjectRepository subjectRepository = new SubjectRepository();
        private readonly TeacherRepository teacherRepository = new TeacherRepository();
        private readonly DateDimensionRepository dateDimensionRepository = new DateDimensionRepository();
        private readonly UniversityExamsFactRepository universityExamsFactRepository = new UniversityExamsFactRepository();

        public void AddAcademicPosition(String positionName)
        {
            academicPositionRepository.Add(new AcademicPosition {AcademicPositionID = positionName});
        }

        public List<AcademicPosition> GetAllAcademicPositions()
        {
            return academicPositionRepository.GetAll();
        }

        public void AddCathedra(String shortName, String longName)
        {
            cathedraRepository.Add(new Cathedra {CathedraID = shortName, NameCathedra = longName});
        }

        public List<Cathedra> GetAllCathedras()
        {
            return cathedraRepository.GetAll();
        }

        public void AddFaculty(String shortName, String longName)
        {
            facultyRepository.Add(new Faculty {FacultyID = shortName, NameFaculty = longName});
        }

        public List<Faculty> GetAllFaculties()
        {
            return facultyRepository.GetAll();
        }

        public void AddGroupOfStudent(Faculty faculty, Speciality speciality, String groupNumber)
        {
            groupOfStudentRepository.Add(new GroupOfStudents
            {
                FacultyID = faculty.FacultyID,
                SpecialtyID = speciality.SpecialtyID,
                NumberGroupID = groupNumber
            });
        }

        public List<GroupOfStudents> GetAllGroups()
        {
            return groupOfStudentRepository.GetAll().OrderBy(group => group.FacultyID).ThenBy(group => group.SpecialtyID).ThenBy(group => group.NumberGroupID).ToList();
        }

        public void AddScientificDegree(String degreeName)
        {
            scientificDegreeRepository.Add(new ScientificDegree {ScientificDegreeID = degreeName});
        }

        public List<ScientificDegree> GetAllScientificDegrees()
        {
            return scientificDegreeRepository.GetAll();
        }

        public void AddSpeciality(Faculty faculty, String shortName, String longName)
        {
            specialityRepository.Add(new Speciality {Faculty = faculty, SpecialtyID = shortName, NameSpecialty = longName});
        }

        public List<Speciality> GetAllSpecialities()
        {
            return specialityRepository.GetAll();
        }

        public List<Speciality> GetSpecialitiesByFaculty(Faculty faculty)
        {
            return specialityRepository.GetBy(speciality => speciality.FacultyID.Equals(speciality.SpecialtyID));
        }

        public List<Student> GetAllStudents()
        {
            return studentRepository.GetAll();
        }

        public void AddStudent(String studentName, String sexStudent, GroupOfStudents groupOfStudents)
        {
            studentRepository.Add(new Student
            {
                FacultyID = groupOfStudents.FacultyID,
                NameStudent = studentName,
                NumberGroupID = groupOfStudents.NumberGroupID,
                SpecialtyID = groupOfStudents.SpecialtyID,
                SexStudent = sexStudent
            });
        }

        public void AddSubject(String shortName, String longName)
        {
            subjectRepository.Add(new Subject {SubjectID = shortName, NameSubject = longName});
        }

        public List<Subject> GetAllSubjects()
        {
            return subjectRepository.GetAll();
        }

        public void AddTeacher(String teacherName, Cathedra cathedra, AcademicPosition academicPosition, ScientificDegree scientificDegree)
        {
            teacherRepository.Add(new Teacher
            {
                NameTeacher = teacherName,
                AcademicPositionID = academicPosition.AcademicPositionID,
                ScientificDegreeID = scientificDegree.ScientificDegreeID
            });
        }

        public DateTime GetMinDate()
        {
            return dateDimensionRepository.GetAll().Min(date => date.DateID);
        }

        public DateTime GetMaxDate()
        {
            return dateDimensionRepository.GetAll().Max(date => date.DateID);
        }

        public Dictionary<Int32, Double> GetDataForChart(DateTime minDate, DateTime maxDate)
        {
            var dictionary = new Dictionary<Int32, Double>();
            var minYear = minDate.Year;
            var maxYear = maxDate.Year;
            for (int i = minYear; i <= maxYear; i++)
            {
                dictionary.Add(i, universityExamsFactRepository.GetBy(fact => fact.DateID.Year.Equals(i)).Average(fact => fact.Mark));
            }

            return dictionary;
        }

        public Dictionary<String, Dictionary<String, Int32>> GetDataForSliceForFacultyAndSexOfStudents()
        {
            var dictionary = new Dictionary<String, Dictionary<String, Int32>>();
            var faculties = facultyRepository.GetAll();
            var maleDictionary = new Dictionary<String, Int32>();
            var femaleDictionary = new Dictionary<String, Int32>();

            foreach (var faculty in faculties)
            {
                maleDictionary.Add(faculty.NameFaculty, studentRepository.GetBy(student => student.FacultyID.Equals(faculty.FacultyID) && student.SexStudent.Equals("Мужской")).Count);
                femaleDictionary.Add(faculty.NameFaculty, studentRepository.GetBy(student => student.FacultyID.Equals(faculty.FacultyID) && student.SexStudent.Equals("Женский")).Count);
            }

            dictionary.Add("Мужской", maleDictionary);
            dictionary.Add("Женский", femaleDictionary);

            return dictionary;
        }

        public Dictionary<Int32, List<UniversityExamsFact>> GetFactsByYears(Int32 startYear, Int32 endYear)
        {
            var dictionary = new Dictionary<Int32, List<UniversityExamsFact>>();
            for (int i = startYear; i <= endYear; i++)
            {
                dictionary.Add(i, universityExamsFactRepository.GetBy(fact => fact.DateID.Year == i));
            }
            return dictionary;
        }
    }
}

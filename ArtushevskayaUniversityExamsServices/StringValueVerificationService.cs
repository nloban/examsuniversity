﻿using System;

namespace ArtushevskayaUniversityExamsServices
{
    public class StringValueVerificationService
    {
        public static bool IsStringValueCorrect(String value)
        {
            return !String.IsNullOrWhiteSpace(value) && !value.StartsWith(" ") &&
                   !value.EndsWith(" ");
        }
    }
}

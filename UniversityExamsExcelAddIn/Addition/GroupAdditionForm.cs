﻿using System.Linq;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class GroupAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public GroupAdditionForm()
        {
            InitializeComponent();
        }

        private void AddGroupButton_Click(object sender, System.EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(GroupTextBox.Text) && FacultyСomboBox.SelectedItem != null && SpecialtyComboBox.SelectedItem != null)
            {
                try
                {
                    AddGroupButton.Enabled = false; 
                    dimensionService.AddGroupOfStudent((Faculty)FacultyСomboBox.SelectedItem, (Speciality)SpecialtyComboBox.SelectedItem, GroupTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная группа уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddGroupButton.Enabled = true;
                }
            }
        }

        private void GroupAdditionForm_Load(object sender, System.EventArgs e)
        {
            InitializeSpecialtyComboBox();
            InitializeFacultyСomboBox();
        }

        private void InitializeSpecialtyComboBox()
        {
            var specialities = dimensionService.GetAllSpecialities();

            foreach (var specialty in specialities.Select((x, i) => new { Index = i, Value = x }))
            {
                SpecialtyComboBox.Items.Insert(specialty.Index, specialty.Value);
            }
        }

        private void InitializeFacultyСomboBox()
        {
            var faculties = dimensionService.GetAllFaculties();

            foreach (var faculty in faculties.Select((x, i) => new { Index = i, Value = x }))
            {
                FacultyСomboBox.Items.Insert(faculty.Index, faculty.Value);
            }
        }

        private void SpecialtyComboBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (SpecialtyComboBox.SelectedItem != null)
            {
                FacultyСomboBox.SelectedItem = ((Speciality) SpecialtyComboBox.SelectedItem).Faculty;
            }
        }

        private void FacultyСomboBox_SelectionChangeCommitted(object sender, System.EventArgs e)
        {
            if (FacultyСomboBox.SelectedItem != null)
            {
                if (((Faculty) FacultyСomboBox.SelectedItem).Specialities.Count != 1)
                {
                    SpecialtyComboBox.Items.Clear();
                    foreach (var speciality in ((Faculty)FacultyСomboBox.SelectedItem).Specialities.Select((x, i) => new { Index = i, Value = x }))
                    {
                        SpecialtyComboBox.Items.Insert(speciality.Index, speciality.Value);
                    }
                }
                else
                {
                    SpecialtyComboBox.SelectedItem =
                        ((Faculty) FacultyСomboBox.SelectedItem).Specialities.FirstOrDefault();
                }
            }
        }
    }
}

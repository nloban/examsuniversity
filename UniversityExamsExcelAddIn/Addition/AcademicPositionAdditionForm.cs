﻿using System;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class AcademicPositionAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public AcademicPositionAdditionForm()
        {
            InitializeComponent();
        }

        private void AddAcademicPositionButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(AcademicPositionTextBox.Text))
            {
                try
                {
                    AddAcademicPositionButton.Enabled = false;
                    dimensionService.AddAcademicPosition(AcademicPositionTextBox.Text); 
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная академическая должность уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddAcademicPositionButton.Enabled = true;
                }
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;
using Telerik.OpenAccess.Metadata.Validation;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class SpecialtyAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();
        public SpecialtyAdditionForm()
        {
            InitializeComponent();
        }

        private void SpecialtyAdditionForm_Load(object sender, EventArgs e)
        {
            var faculties = dimensionService.GetAllFaculties();

            foreach (var faculty in faculties.Select((x, i) => new { Index = i, Value = x }))
            {
                comboBox1.Items.Insert(faculty.Index, faculty.Value);
            }
        }

        private void AddSpecialtyButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(SpecialtyTextBox.Text) 
                && StringValueVerificationService.IsStringValueCorrect(ShortNameTextBox.Text) 
                 && comboBox1.SelectedItem != null)
            {
                try
                {
                    AddSpecialtyButton.Enabled = false;
                    dimensionService.AddSpeciality((Faculty)comboBox1.SelectedItem, ShortNameTextBox.Text, SpecialtyTextBox.Text); 
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная специальность уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddSpecialtyButton.Enabled = true;
                }
            }
        }
    }
}

﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class ScientificDegreeAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ScientificDegreeLabel = new System.Windows.Forms.Label();
            this.ScientificDegreeTextBox = new System.Windows.Forms.TextBox();
            this.AddScientificDegreeButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ScientificDegreeLabel
            // 
            this.ScientificDegreeLabel.AutoSize = true;
            this.ScientificDegreeLabel.Location = new System.Drawing.Point(13, 30);
            this.ScientificDegreeLabel.Name = "ScientificDegreeLabel";
            this.ScientificDegreeLabel.Size = new System.Drawing.Size(138, 13);
            this.ScientificDegreeLabel.TabIndex = 0;
            this.ScientificDegreeLabel.Text = "Название ученой степени";
            // 
            // ScientificDegreeTextBox
            // 
            this.ScientificDegreeTextBox.Location = new System.Drawing.Point(156, 30);
            this.ScientificDegreeTextBox.Name = "ScientificDegreeTextBox";
            this.ScientificDegreeTextBox.Size = new System.Drawing.Size(362, 20);
            this.ScientificDegreeTextBox.TabIndex = 1;
            // 
            // AddScientificDegreeButton
            // 
            this.AddScientificDegreeButton.Location = new System.Drawing.Point(443, 61);
            this.AddScientificDegreeButton.Name = "AddScientificDegreeButton";
            this.AddScientificDegreeButton.Size = new System.Drawing.Size(75, 23);
            this.AddScientificDegreeButton.TabIndex = 2;
            this.AddScientificDegreeButton.Text = "Добавить";
            this.AddScientificDegreeButton.UseVisualStyleBackColor = true;
            this.AddScientificDegreeButton.Click += new System.EventHandler(this.AddScientificDegreeButton_Click);
            // 
            // ScientificDegreeAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(530, 96);
            this.Controls.Add(this.AddScientificDegreeButton);
            this.Controls.Add(this.ScientificDegreeTextBox);
            this.Controls.Add(this.ScientificDegreeLabel);
            this.Name = "ScientificDegreeAdditionForm";
            this.Text = "Добавление ученой степени";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label ScientificDegreeLabel;
        private System.Windows.Forms.TextBox ScientificDegreeTextBox;
        private System.Windows.Forms.Button AddScientificDegreeButton;
    }
}
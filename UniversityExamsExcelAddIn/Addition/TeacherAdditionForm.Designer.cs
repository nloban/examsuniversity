﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class TeacherAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddTeacherButton = new System.Windows.Forms.Button();
            this.TeacherLabel = new System.Windows.Forms.Label();
            this.TeacherTextBox = new System.Windows.Forms.TextBox();
            this.CathedraLabel = new System.Windows.Forms.Label();
            this.AcademicPositionLabel = new System.Windows.Forms.Label();
            this.ScientificDegreeLabel = new System.Windows.Forms.Label();
            this.CathedraСomboBox = new System.Windows.Forms.ComboBox();
            this.AcademicPositionComboBox = new System.Windows.Forms.ComboBox();
            this.ScientificDegreeСomboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // AddTeacherButton
            // 
            this.AddTeacherButton.Location = new System.Drawing.Point(471, 171);
            this.AddTeacherButton.Name = "AddTeacherButton";
            this.AddTeacherButton.Size = new System.Drawing.Size(75, 23);
            this.AddTeacherButton.TabIndex = 0;
            this.AddTeacherButton.Text = "Добавить";
            this.AddTeacherButton.UseVisualStyleBackColor = true;
            this.AddTeacherButton.Click += new System.EventHandler(this.AddTeacherButton_Click);
            // 
            // TeacherLabel
            // 
            this.TeacherLabel.AutoSize = true;
            this.TeacherLabel.Location = new System.Drawing.Point(12, 42);
            this.TeacherLabel.Name = "TeacherLabel";
            this.TeacherLabel.Size = new System.Drawing.Size(116, 13);
            this.TeacherLabel.TabIndex = 1;
            this.TeacherLabel.Text = "ФИО Преподавателя";
            // 
            // TeacherTextBox
            // 
            this.TeacherTextBox.Location = new System.Drawing.Point(183, 39);
            this.TeacherTextBox.Name = "TeacherTextBox";
            this.TeacherTextBox.Size = new System.Drawing.Size(363, 20);
            this.TeacherTextBox.TabIndex = 2;
            // 
            // CathedraLabel
            // 
            this.CathedraLabel.AutoSize = true;
            this.CathedraLabel.Location = new System.Drawing.Point(12, 70);
            this.CathedraLabel.Name = "CathedraLabel";
            this.CathedraLabel.Size = new System.Drawing.Size(52, 13);
            this.CathedraLabel.TabIndex = 3;
            this.CathedraLabel.Text = "Кафедра";
            // 
            // AcademicPositionLabel
            // 
            this.AcademicPositionLabel.AutoSize = true;
            this.AcademicPositionLabel.Location = new System.Drawing.Point(12, 104);
            this.AcademicPositionLabel.Name = "AcademicPositionLabel";
            this.AcademicPositionLabel.Size = new System.Drawing.Size(145, 13);
            this.AcademicPositionLabel.TabIndex = 4;
            this.AcademicPositionLabel.Text = "Академическая должность";
            // 
            // ScientificDegreeLabel
            // 
            this.ScientificDegreeLabel.AutoSize = true;
            this.ScientificDegreeLabel.Location = new System.Drawing.Point(12, 133);
            this.ScientificDegreeLabel.Name = "ScientificDegreeLabel";
            this.ScientificDegreeLabel.Size = new System.Drawing.Size(88, 13);
            this.ScientificDegreeLabel.TabIndex = 5;
            this.ScientificDegreeLabel.Text = "Ученая степень";
            // 
            // CathedraСomboBox
            // 
            this.CathedraСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CathedraСomboBox.FormattingEnabled = true;
            this.CathedraСomboBox.Location = new System.Drawing.Point(183, 67);
            this.CathedraСomboBox.Name = "CathedraСomboBox";
            this.CathedraСomboBox.Size = new System.Drawing.Size(363, 21);
            this.CathedraСomboBox.TabIndex = 6;
            // 
            // AcademicPositionComboBox
            // 
            this.AcademicPositionComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.AcademicPositionComboBox.FormattingEnabled = true;
            this.AcademicPositionComboBox.Location = new System.Drawing.Point(183, 101);
            this.AcademicPositionComboBox.Name = "AcademicPositionComboBox";
            this.AcademicPositionComboBox.Size = new System.Drawing.Size(363, 21);
            this.AcademicPositionComboBox.TabIndex = 7;
            // 
            // ScientificDegreeСomboBox
            // 
            this.ScientificDegreeСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ScientificDegreeСomboBox.FormattingEnabled = true;
            this.ScientificDegreeСomboBox.Location = new System.Drawing.Point(183, 130);
            this.ScientificDegreeСomboBox.Name = "ScientificDegreeСomboBox";
            this.ScientificDegreeСomboBox.Size = new System.Drawing.Size(363, 21);
            this.ScientificDegreeСomboBox.TabIndex = 8;
            // 
            // TeacherAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(558, 210);
            this.Controls.Add(this.ScientificDegreeСomboBox);
            this.Controls.Add(this.AcademicPositionComboBox);
            this.Controls.Add(this.CathedraСomboBox);
            this.Controls.Add(this.ScientificDegreeLabel);
            this.Controls.Add(this.AcademicPositionLabel);
            this.Controls.Add(this.CathedraLabel);
            this.Controls.Add(this.TeacherTextBox);
            this.Controls.Add(this.TeacherLabel);
            this.Controls.Add(this.AddTeacherButton);
            this.Name = "TeacherAdditionForm";
            this.Text = "Добавление преподавателя";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddTeacherButton;
        private System.Windows.Forms.Label TeacherLabel;
        private System.Windows.Forms.TextBox TeacherTextBox;
        private System.Windows.Forms.Label CathedraLabel;
        private System.Windows.Forms.Label AcademicPositionLabel;
        private System.Windows.Forms.Label ScientificDegreeLabel;
        private System.Windows.Forms.ComboBox CathedraСomboBox;
        private System.Windows.Forms.ComboBox AcademicPositionComboBox;
        private System.Windows.Forms.ComboBox ScientificDegreeСomboBox;
    }
}
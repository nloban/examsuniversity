﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class CathedraAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddCathedraButton = new System.Windows.Forms.Button();
            this.CathedraTextBox = new System.Windows.Forms.TextBox();
            this.CathedraLabel = new System.Windows.Forms.Label();
            this.ShortLabel = new System.Windows.Forms.Label();
            this.ShortNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // AddCathedraButton
            // 
            this.AddCathedraButton.Location = new System.Drawing.Point(431, 87);
            this.AddCathedraButton.Name = "AddCathedraButton";
            this.AddCathedraButton.Size = new System.Drawing.Size(75, 23);
            this.AddCathedraButton.TabIndex = 0;
            this.AddCathedraButton.Text = "Добавить";
            this.AddCathedraButton.UseVisualStyleBackColor = true;
            this.AddCathedraButton.Click += new System.EventHandler(this.AddCathedraButton_Click);
            // 
            // CathedraTextBox
            // 
            this.CathedraTextBox.Location = new System.Drawing.Point(192, 52);
            this.CathedraTextBox.Name = "CathedraTextBox";
            this.CathedraTextBox.Size = new System.Drawing.Size(314, 20);
            this.CathedraTextBox.TabIndex = 1;
            // 
            // CathedraLabel
            // 
            this.CathedraLabel.AutoSize = true;
            this.CathedraLabel.Location = new System.Drawing.Point(13, 55);
            this.CathedraLabel.Name = "CathedraLabel";
            this.CathedraLabel.Size = new System.Drawing.Size(145, 13);
            this.CathedraLabel.TabIndex = 2;
            this.CathedraLabel.Text = "Полное название кафедры";
            // 
            // ShortLabel
            // 
            this.ShortLabel.AutoSize = true;
            this.ShortLabel.Location = new System.Drawing.Point(13, 20);
            this.ShortLabel.Name = "ShortLabel";
            this.ShortLabel.Size = new System.Drawing.Size(149, 13);
            this.ShortLabel.TabIndex = 3;
            this.ShortLabel.Text = "Краткое название кафедры";
            // 
            // ShortNameTextBox
            // 
            this.ShortNameTextBox.Location = new System.Drawing.Point(192, 17);
            this.ShortNameTextBox.Name = "ShortNameTextBox";
            this.ShortNameTextBox.Size = new System.Drawing.Size(314, 20);
            this.ShortNameTextBox.TabIndex = 4;
            // 
            // CathedraAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(513, 122);
            this.Controls.Add(this.ShortNameTextBox);
            this.Controls.Add(this.ShortLabel);
            this.Controls.Add(this.CathedraLabel);
            this.Controls.Add(this.CathedraTextBox);
            this.Controls.Add(this.AddCathedraButton);
            this.Name = "CathedraAdditionForm";
            this.Text = "Добавление кафедры";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddCathedraButton;
        private System.Windows.Forms.TextBox CathedraTextBox;
        private System.Windows.Forms.Label CathedraLabel;
        private System.Windows.Forms.Label ShortLabel;
        private System.Windows.Forms.TextBox ShortNameTextBox;
    }
}
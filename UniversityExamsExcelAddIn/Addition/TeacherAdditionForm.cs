﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class TeacherAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public TeacherAdditionForm()
        {
            InitializeComponent();
            InitializeAcademicPositionComboBox();
            InitializeCathedraСomboBox();
            InitializeScientificDegreeСomboBox();
        }

        private void InitializeAcademicPositionComboBox()
        {
            var academicPositions = dimensionService.GetAllAcademicPositions();

            foreach (var academicPosition in academicPositions.Select((x, i) => new { Index = i, Value = x }))
            {
                AcademicPositionComboBox.Items.Insert(academicPosition.Index, academicPosition.Value);
            }
        }

        private void InitializeCathedraСomboBox()
        {
            var cathedras = dimensionService.GetAllCathedras();

            foreach (var cathedra in cathedras.Select((x, i) => new { Index = i, Value = x }))
            {
                CathedraСomboBox.Items.Insert(cathedra.Index, cathedra.Value);
            }
        }

        private void InitializeScientificDegreeСomboBox()
        {
            var scientificDegrees = dimensionService.GetAllScientificDegrees();

            foreach (var scientificDegree in scientificDegrees.Select((x, i) => new { Index = i, Value = x }))
            {
                ScientificDegreeСomboBox.Items.Insert(scientificDegree.Index, scientificDegree.Value);
            }
        }

        private void AddTeacherButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(TeacherTextBox.Text)
                && ScientificDegreeСomboBox.SelectedItem != null && CathedraСomboBox.SelectedItem != null &&
                AcademicPositionComboBox.SelectedItem != null)
            {
                AddTeacherButton.Enabled = false;
                dimensionService.AddTeacher(TeacherTextBox.Text, (Cathedra) CathedraСomboBox.SelectedItem, (AcademicPosition) AcademicPositionComboBox.SelectedItem, (ScientificDegree) ScientificDegreeСomboBox.SelectedItem);
                Close();
            }
        }
    }
}

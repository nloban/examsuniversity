﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class GroupAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FacultyLabel = new System.Windows.Forms.Label();
            this.SpecialtyLabel = new System.Windows.Forms.Label();
            this.GroupLabel = new System.Windows.Forms.Label();
            this.FacultyСomboBox = new System.Windows.Forms.ComboBox();
            this.SpecialtyComboBox = new System.Windows.Forms.ComboBox();
            this.GroupTextBox = new System.Windows.Forms.TextBox();
            this.AddGroupButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // FacultyLabel
            // 
            this.FacultyLabel.AutoSize = true;
            this.FacultyLabel.Location = new System.Drawing.Point(13, 41);
            this.FacultyLabel.Name = "FacultyLabel";
            this.FacultyLabel.Size = new System.Drawing.Size(158, 13);
            this.FacultyLabel.TabIndex = 0;
            this.FacultyLabel.Text = "Полное название факультета";
            // 
            // SpecialtyLabel
            // 
            this.SpecialtyLabel.AutoSize = true;
            this.SpecialtyLabel.Location = new System.Drawing.Point(13, 75);
            this.SpecialtyLabel.Name = "SpecialtyLabel";
            this.SpecialtyLabel.Size = new System.Drawing.Size(176, 13);
            this.SpecialtyLabel.TabIndex = 1;
            this.SpecialtyLabel.Text = "Полное название специальности";
            // 
            // GroupLabel
            // 
            this.GroupLabel.AutoSize = true;
            this.GroupLabel.Location = new System.Drawing.Point(13, 113);
            this.GroupLabel.Name = "GroupLabel";
            this.GroupLabel.Size = new System.Drawing.Size(80, 13);
            this.GroupLabel.TabIndex = 2;
            this.GroupLabel.Text = "Номер группы";
            // 
            // FacultyСomboBox
            // 
            this.FacultyСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FacultyСomboBox.FormattingEnabled = true;
            this.FacultyСomboBox.Location = new System.Drawing.Point(212, 38);
            this.FacultyСomboBox.Name = "FacultyСomboBox";
            this.FacultyСomboBox.Size = new System.Drawing.Size(463, 21);
            this.FacultyСomboBox.TabIndex = 3;
            this.FacultyСomboBox.SelectionChangeCommitted += new System.EventHandler(this.FacultyСomboBox_SelectionChangeCommitted);
            // 
            // SpecialtyComboBox
            // 
            this.SpecialtyComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SpecialtyComboBox.FormattingEnabled = true;
            this.SpecialtyComboBox.Location = new System.Drawing.Point(212, 72);
            this.SpecialtyComboBox.Name = "SpecialtyComboBox";
            this.SpecialtyComboBox.Size = new System.Drawing.Size(463, 21);
            this.SpecialtyComboBox.TabIndex = 4;
            this.SpecialtyComboBox.SelectionChangeCommitted += new System.EventHandler(this.SpecialtyComboBox_SelectionChangeCommitted);
            // 
            // GroupTextBox
            // 
            this.GroupTextBox.Location = new System.Drawing.Point(212, 110);
            this.GroupTextBox.Name = "GroupTextBox";
            this.GroupTextBox.Size = new System.Drawing.Size(463, 20);
            this.GroupTextBox.TabIndex = 5;
            // 
            // AddGroupButton
            // 
            this.AddGroupButton.Location = new System.Drawing.Point(600, 149);
            this.AddGroupButton.Name = "AddGroupButton";
            this.AddGroupButton.Size = new System.Drawing.Size(75, 23);
            this.AddGroupButton.TabIndex = 6;
            this.AddGroupButton.Text = "Добавить";
            this.AddGroupButton.UseVisualStyleBackColor = true;
            this.AddGroupButton.Click += new System.EventHandler(this.AddGroupButton_Click);
            // 
            // GroupAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 183);
            this.Controls.Add(this.AddGroupButton);
            this.Controls.Add(this.GroupTextBox);
            this.Controls.Add(this.SpecialtyComboBox);
            this.Controls.Add(this.FacultyСomboBox);
            this.Controls.Add(this.GroupLabel);
            this.Controls.Add(this.SpecialtyLabel);
            this.Controls.Add(this.FacultyLabel);
            this.Name = "GroupAdditionForm";
            this.Text = "Добавление группы";
            this.Load += new System.EventHandler(this.GroupAdditionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FacultyLabel;
        private System.Windows.Forms.Label SpecialtyLabel;
        private System.Windows.Forms.Label GroupLabel;
        private System.Windows.Forms.ComboBox FacultyСomboBox;
        private System.Windows.Forms.ComboBox SpecialtyComboBox;
        private System.Windows.Forms.TextBox GroupTextBox;
        private System.Windows.Forms.Button AddGroupButton;
    }
}
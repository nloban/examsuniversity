﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class SpecialtyAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FacultyLabel = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.SpecialtyLabel = new System.Windows.Forms.Label();
            this.SpecialtyTextBox = new System.Windows.Forms.TextBox();
            this.AddSpecialtyButton = new System.Windows.Forms.Button();
            this.ShortNameTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // FacultyLabel
            // 
            this.FacultyLabel.AutoSize = true;
            this.FacultyLabel.Location = new System.Drawing.Point(12, 23);
            this.FacultyLabel.Name = "FacultyLabel";
            this.FacultyLabel.Size = new System.Drawing.Size(158, 13);
            this.FacultyLabel.TabIndex = 0;
            this.FacultyLabel.Text = "Полное название факультета";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(202, 23);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(451, 21);
            this.comboBox1.TabIndex = 1;
            // 
            // SpecialtyLabel
            // 
            this.SpecialtyLabel.AutoSize = true;
            this.SpecialtyLabel.Location = new System.Drawing.Point(12, 100);
            this.SpecialtyLabel.Name = "SpecialtyLabel";
            this.SpecialtyLabel.Size = new System.Drawing.Size(176, 13);
            this.SpecialtyLabel.TabIndex = 2;
            this.SpecialtyLabel.Text = "Полное название специальности";
            // 
            // SpecialtyTextBox
            // 
            this.SpecialtyTextBox.Location = new System.Drawing.Point(202, 97);
            this.SpecialtyTextBox.Name = "SpecialtyTextBox";
            this.SpecialtyTextBox.Size = new System.Drawing.Size(451, 20);
            this.SpecialtyTextBox.TabIndex = 3;
            // 
            // AddSpecialtyButton
            // 
            this.AddSpecialtyButton.Location = new System.Drawing.Point(578, 123);
            this.AddSpecialtyButton.Name = "AddSpecialtyButton";
            this.AddSpecialtyButton.Size = new System.Drawing.Size(75, 23);
            this.AddSpecialtyButton.TabIndex = 4;
            this.AddSpecialtyButton.Text = "Добавить";
            this.AddSpecialtyButton.UseVisualStyleBackColor = true;
            this.AddSpecialtyButton.Click += new System.EventHandler(this.AddSpecialtyButton_Click);
            // 
            // ShortNameTextBox
            // 
            this.ShortNameTextBox.Location = new System.Drawing.Point(202, 62);
            this.ShortNameTextBox.Name = "ShortNameTextBox";
            this.ShortNameTextBox.Size = new System.Drawing.Size(451, 20);
            this.ShortNameTextBox.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(180, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Краткое название специальности";
            // 
            // SpecialtyAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(665, 161);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ShortNameTextBox);
            this.Controls.Add(this.AddSpecialtyButton);
            this.Controls.Add(this.SpecialtyTextBox);
            this.Controls.Add(this.SpecialtyLabel);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.FacultyLabel);
            this.Name = "SpecialtyAdditionForm";
            this.Text = "Добавление специальности";
            this.Load += new System.EventHandler(this.SpecialtyAdditionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FacultyLabel;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label SpecialtyLabel;
        private System.Windows.Forms.TextBox SpecialtyTextBox;
        private System.Windows.Forms.Button AddSpecialtyButton;
        private System.Windows.Forms.TextBox ShortNameTextBox;
        private System.Windows.Forms.Label label1;
    }
}
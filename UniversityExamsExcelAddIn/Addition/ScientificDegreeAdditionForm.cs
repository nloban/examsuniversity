﻿using System;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class ScientificDegreeAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public ScientificDegreeAdditionForm()
        {
            InitializeComponent();
        }

        private void AddScientificDegreeButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ScientificDegreeTextBox.Text))
            {
                try
                {
                    AddScientificDegreeButton.Enabled = false;
                    dimensionService.AddScientificDegree(ScientificDegreeTextBox.Text);
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная ученая степень уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddScientificDegreeButton.Enabled = true;
                }
            }
        }
    }
}

﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class FacultyAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FacultyNameLabel = new System.Windows.Forms.Label();
            this.AddFacultyButton = new System.Windows.Forms.Button();
            this.FacultyNameTextBox = new System.Windows.Forms.TextBox();
            this.ShortNameLabel = new System.Windows.Forms.Label();
            this.ShortNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // FacultyNameLabel
            // 
            this.FacultyNameLabel.AutoSize = true;
            this.FacultyNameLabel.Location = new System.Drawing.Point(12, 69);
            this.FacultyNameLabel.Name = "FacultyNameLabel";
            this.FacultyNameLabel.Size = new System.Drawing.Size(158, 13);
            this.FacultyNameLabel.TabIndex = 0;
            this.FacultyNameLabel.Text = "Полное название факультета";
            // 
            // AddFacultyButton
            // 
            this.AddFacultyButton.Location = new System.Drawing.Point(573, 103);
            this.AddFacultyButton.Name = "AddFacultyButton";
            this.AddFacultyButton.Size = new System.Drawing.Size(75, 23);
            this.AddFacultyButton.TabIndex = 1;
            this.AddFacultyButton.Text = "Добавить";
            this.AddFacultyButton.UseVisualStyleBackColor = true;
            this.AddFacultyButton.Click += new System.EventHandler(this.AddFacultyButton_Click);
            // 
            // FacultyNameTextBox
            // 
            this.FacultyNameTextBox.Location = new System.Drawing.Point(199, 66);
            this.FacultyNameTextBox.Name = "FacultyNameTextBox";
            this.FacultyNameTextBox.Size = new System.Drawing.Size(449, 20);
            this.FacultyNameTextBox.TabIndex = 2;
            // 
            // ShortNameLabel
            // 
            this.ShortNameLabel.AutoSize = true;
            this.ShortNameLabel.Location = new System.Drawing.Point(12, 24);
            this.ShortNameLabel.Name = "ShortNameLabel";
            this.ShortNameLabel.Size = new System.Drawing.Size(162, 13);
            this.ShortNameLabel.TabIndex = 3;
            this.ShortNameLabel.Text = "Краткое название факультета";
            // 
            // ShortNameTextBox
            // 
            this.ShortNameTextBox.Location = new System.Drawing.Point(199, 21);
            this.ShortNameTextBox.Name = "ShortNameTextBox";
            this.ShortNameTextBox.Size = new System.Drawing.Size(449, 20);
            this.ShortNameTextBox.TabIndex = 4;
            // 
            // FacultyAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 138);
            this.Controls.Add(this.ShortNameTextBox);
            this.Controls.Add(this.ShortNameLabel);
            this.Controls.Add(this.FacultyNameTextBox);
            this.Controls.Add(this.AddFacultyButton);
            this.Controls.Add(this.FacultyNameLabel);
            this.Name = "FacultyAdditionForm";
            this.Text = "Добавление факультета";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label FacultyNameLabel;
        private System.Windows.Forms.Button AddFacultyButton;
        private System.Windows.Forms.TextBox FacultyNameTextBox;
        private System.Windows.Forms.Label ShortNameLabel;
        private System.Windows.Forms.TextBox ShortNameTextBox;
    }
}
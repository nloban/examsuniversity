﻿using System;
using System.Linq;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class StudentAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public StudentAdditionForm()
        {
            InitializeComponent();
        }

        private void AddStudentВutton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(NameStudentTextBox.Text)
                && SexStudentComboBox.SelectedItem != null && GroupСomboBox.SelectedItem != null)
            {
                AddStudentВutton.Enabled = false;
                dimensionService.AddStudent(NameStudentTextBox.Text, SexStudentComboBox.SelectedItem.ToString(), (GroupOfStudents) GroupСomboBox.SelectedItem);
                Close();
            }
        }

        private void StudentAdditionForm_Load(object sender, EventArgs e)
        {
            SexStudentComboBox.Items.AddRange(new object[] { "Женский", "Мужской" });

            var groups = dimensionService.GetAllGroups();

            foreach (var group in groups.Select((x, i) => new { Index = i, Value = x }))
            {
                GroupСomboBox.Items.Insert(group.Index, group.Value);
            }
        }
    }
}

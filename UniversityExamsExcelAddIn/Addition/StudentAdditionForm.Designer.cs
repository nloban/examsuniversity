﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class StudentAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NameStudentLabel = new System.Windows.Forms.Label();
            this.SexStudentLabel = new System.Windows.Forms.Label();
            this.GroupLabel = new System.Windows.Forms.Label();
            this.AddStudentВutton = new System.Windows.Forms.Button();
            this.NameStudentTextBox = new System.Windows.Forms.TextBox();
            this.GroupСomboBox = new System.Windows.Forms.ComboBox();
            this.SexStudentComboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // NameStudentLabel
            // 
            this.NameStudentLabel.AutoSize = true;
            this.NameStudentLabel.Location = new System.Drawing.Point(13, 36);
            this.NameStudentLabel.Name = "NameStudentLabel";
            this.NameStudentLabel.Size = new System.Drawing.Size(82, 13);
            this.NameStudentLabel.TabIndex = 0;
            this.NameStudentLabel.Text = "ФИО студента";
            // 
            // SexStudentLabel
            // 
            this.SexStudentLabel.AutoSize = true;
            this.SexStudentLabel.Location = new System.Drawing.Point(13, 68);
            this.SexStudentLabel.Name = "SexStudentLabel";
            this.SexStudentLabel.Size = new System.Drawing.Size(75, 13);
            this.SexStudentLabel.TabIndex = 1;
            this.SexStudentLabel.Text = "Пол студента";
            // 
            // GroupLabel
            // 
            this.GroupLabel.AutoSize = true;
            this.GroupLabel.Location = new System.Drawing.Point(13, 99);
            this.GroupLabel.Name = "GroupLabel";
            this.GroupLabel.Size = new System.Drawing.Size(80, 13);
            this.GroupLabel.TabIndex = 2;
            this.GroupLabel.Text = "Номер группы";
            // 
            // AddStudentВutton
            // 
            this.AddStudentВutton.Location = new System.Drawing.Point(266, 136);
            this.AddStudentВutton.Name = "AddStudentВutton";
            this.AddStudentВutton.Size = new System.Drawing.Size(75, 23);
            this.AddStudentВutton.TabIndex = 3;
            this.AddStudentВutton.Text = "Добавить";
            this.AddStudentВutton.UseVisualStyleBackColor = true;
            this.AddStudentВutton.Click += new System.EventHandler(this.AddStudentВutton_Click);
            // 
            // NameStudentTextBox
            // 
            this.NameStudentTextBox.Location = new System.Drawing.Point(122, 33);
            this.NameStudentTextBox.Name = "NameStudentTextBox";
            this.NameStudentTextBox.Size = new System.Drawing.Size(219, 20);
            this.NameStudentTextBox.TabIndex = 4;
            // 
            // GroupСomboBox
            // 
            this.GroupСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GroupСomboBox.FormattingEnabled = true;
            this.GroupСomboBox.Location = new System.Drawing.Point(122, 99);
            this.GroupСomboBox.Name = "GroupСomboBox";
            this.GroupСomboBox.Size = new System.Drawing.Size(219, 21);
            this.GroupСomboBox.TabIndex = 6;
            // 
            // SexStudentComboBox
            // 
            this.SexStudentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SexStudentComboBox.FormattingEnabled = true;
            this.SexStudentComboBox.Location = new System.Drawing.Point(122, 65);
            this.SexStudentComboBox.Name = "SexStudentComboBox";
            this.SexStudentComboBox.Size = new System.Drawing.Size(219, 21);
            this.SexStudentComboBox.TabIndex = 9;
            // 
            // StudentAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 175);
            this.Controls.Add(this.SexStudentComboBox);
            this.Controls.Add(this.GroupСomboBox);
            this.Controls.Add(this.NameStudentTextBox);
            this.Controls.Add(this.AddStudentВutton);
            this.Controls.Add(this.GroupLabel);
            this.Controls.Add(this.SexStudentLabel);
            this.Controls.Add(this.NameStudentLabel);
            this.Name = "StudentAdditionForm";
            this.Text = "Добавление студента";
            this.Load += new System.EventHandler(this.StudentAdditionForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NameStudentLabel;
        private System.Windows.Forms.Label SexStudentLabel;
        private System.Windows.Forms.Label GroupLabel;
        private System.Windows.Forms.Button AddStudentВutton;
        private System.Windows.Forms.TextBox NameStudentTextBox;
        private System.Windows.Forms.ComboBox GroupСomboBox;
        private System.Windows.Forms.ComboBox SexStudentComboBox;
    }
}
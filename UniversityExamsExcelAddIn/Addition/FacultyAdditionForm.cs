﻿using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class FacultyAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public FacultyAdditionForm()
        {
            InitializeComponent();
        }

        private void AddFacultyButton_Click(object sender, System.EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ShortNameTextBox.Text)
                && StringValueVerificationService.IsStringValueCorrect(FacultyNameTextBox.Text))
            {
                try
                {
                    AddFacultyButton.Enabled = false;
                    dimensionService.AddFaculty(ShortNameTextBox.Text, FacultyNameTextBox.Text); 
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный факультет уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddFacultyButton.Enabled = true;
                }
            }
        }
    }
}

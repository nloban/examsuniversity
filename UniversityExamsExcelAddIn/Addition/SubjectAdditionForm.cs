﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class SubjectAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public SubjectAdditionForm()
        {
            InitializeComponent();
        }

        private void AddSubjectButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ShortSubjectNameTextBox.Text)
                && StringValueVerificationService.IsStringValueCorrect(SubjectNameTextBox.Text))
            {
                try
                {
                    AddSubjectButton.Enabled = false;
                    dimensionService.AddSubject(ShortSubjectNameTextBox.Text, SubjectNameTextBox.Text); Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данный предмет уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddSubjectButton.Enabled = true;
                }
            }
        }
    }
}

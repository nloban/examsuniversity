﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class UniversityExamsFactAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DataLabel = new System.Windows.Forms.Label();
            this.AddDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.StudentLabel = new System.Windows.Forms.Label();
            this.GroupComboBox = new System.Windows.Forms.ComboBox();
            this.StudentGroupBox = new System.Windows.Forms.GroupBox();
            this.NameSubjectСomboBox = new System.Windows.Forms.ComboBox();
            this.AddMarkТextBox = new System.Windows.Forms.TextBox();
            this.GroupLabel = new System.Windows.Forms.Label();
            this.NameSubjectLabel = new System.Windows.Forms.Label();
            this.MarkLabel = new System.Windows.Forms.Label();
            this.SubjectGroupBox = new System.Windows.Forms.GroupBox();
            this.NameTeacherComboBox = new System.Windows.Forms.ComboBox();
            this.NameTeacherLabel = new System.Windows.Forms.Label();
            this.AddFactButton = new System.Windows.Forms.Button();
            this.NameStudentComboBox = new System.Windows.Forms.ComboBox();
            this.StudentGroupBox.SuspendLayout();
            this.SubjectGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // DataLabel
            // 
            this.DataLabel.AutoSize = true;
            this.DataLabel.Location = new System.Drawing.Point(23, 13);
            this.DataLabel.Name = "DataLabel";
            this.DataLabel.Size = new System.Drawing.Size(96, 13);
            this.DataLabel.TabIndex = 0;
            this.DataLabel.Text = "Дата добавления";
            // 
            // AddDateTimePicker
            // 
            this.AddDateTimePicker.Location = new System.Drawing.Point(186, 13);
            this.AddDateTimePicker.Name = "AddDateTimePicker";
            this.AddDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.AddDateTimePicker.TabIndex = 1;
            // 
            // StudentLabel
            // 
            this.StudentLabel.AutoSize = true;
            this.StudentLabel.Location = new System.Drawing.Point(15, 33);
            this.StudentLabel.Name = "StudentLabel";
            this.StudentLabel.Size = new System.Drawing.Size(82, 13);
            this.StudentLabel.TabIndex = 2;
            this.StudentLabel.Text = "ФИО студента";
            // 
            // GroupComboBox
            // 
            this.GroupComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.GroupComboBox.FormattingEnabled = true;
            this.GroupComboBox.Location = new System.Drawing.Point(160, 65);
            this.GroupComboBox.Name = "GroupComboBox";
            this.GroupComboBox.Size = new System.Drawing.Size(279, 21);
            this.GroupComboBox.TabIndex = 4;
            // 
            // StudentGroupBox
            // 
            this.StudentGroupBox.Controls.Add(this.NameStudentComboBox);
            this.StudentGroupBox.Controls.Add(this.MarkLabel);
            this.StudentGroupBox.Controls.Add(this.GroupLabel);
            this.StudentGroupBox.Controls.Add(this.AddMarkТextBox);
            this.StudentGroupBox.Controls.Add(this.StudentLabel);
            this.StudentGroupBox.Controls.Add(this.GroupComboBox);
            this.StudentGroupBox.Location = new System.Drawing.Point(26, 61);
            this.StudentGroupBox.Name = "StudentGroupBox";
            this.StudentGroupBox.Size = new System.Drawing.Size(445, 154);
            this.StudentGroupBox.TabIndex = 6;
            this.StudentGroupBox.TabStop = false;
            this.StudentGroupBox.Text = "Сведенье о студенте";
            // 
            // NameSubjectСomboBox
            // 
            this.NameSubjectСomboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NameSubjectСomboBox.FormattingEnabled = true;
            this.NameSubjectСomboBox.Location = new System.Drawing.Point(160, 32);
            this.NameSubjectСomboBox.Name = "NameSubjectСomboBox";
            this.NameSubjectСomboBox.Size = new System.Drawing.Size(279, 21);
            this.NameSubjectСomboBox.TabIndex = 5;
            // 
            // AddMarkТextBox
            // 
            this.AddMarkТextBox.Location = new System.Drawing.Point(160, 109);
            this.AddMarkТextBox.Name = "AddMarkТextBox";
            this.AddMarkТextBox.Size = new System.Drawing.Size(279, 20);
            this.AddMarkТextBox.TabIndex = 6;
            // 
            // GroupLabel
            // 
            this.GroupLabel.AutoSize = true;
            this.GroupLabel.Location = new System.Drawing.Point(15, 65);
            this.GroupLabel.Name = "GroupLabel";
            this.GroupLabel.Size = new System.Drawing.Size(80, 13);
            this.GroupLabel.TabIndex = 7;
            this.GroupLabel.Text = "Номер группы";
            // 
            // NameSubjectLabel
            // 
            this.NameSubjectLabel.AutoSize = true;
            this.NameSubjectLabel.Location = new System.Drawing.Point(22, 35);
            this.NameSubjectLabel.Name = "NameSubjectLabel";
            this.NameSubjectLabel.Size = new System.Drawing.Size(122, 13);
            this.NameSubjectLabel.TabIndex = 8;
            this.NameSubjectLabel.Text = "Название дисциплины";
            // 
            // MarkLabel
            // 
            this.MarkLabel.AutoSize = true;
            this.MarkLabel.Location = new System.Drawing.Point(18, 109);
            this.MarkLabel.Name = "MarkLabel";
            this.MarkLabel.Size = new System.Drawing.Size(45, 13);
            this.MarkLabel.TabIndex = 9;
            this.MarkLabel.Text = "Оценка";
            // 
            // SubjectGroupBox
            // 
            this.SubjectGroupBox.Controls.Add(this.NameTeacherLabel);
            this.SubjectGroupBox.Controls.Add(this.NameTeacherComboBox);
            this.SubjectGroupBox.Controls.Add(this.NameSubjectLabel);
            this.SubjectGroupBox.Controls.Add(this.NameSubjectСomboBox);
            this.SubjectGroupBox.Location = new System.Drawing.Point(26, 249);
            this.SubjectGroupBox.Name = "SubjectGroupBox";
            this.SubjectGroupBox.Size = new System.Drawing.Size(445, 100);
            this.SubjectGroupBox.TabIndex = 9;
            this.SubjectGroupBox.TabStop = false;
            this.SubjectGroupBox.Text = "Сведенье о дисциплине";
            // 
            // NameTeacherComboBox
            // 
            this.NameTeacherComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NameTeacherComboBox.FormattingEnabled = true;
            this.NameTeacherComboBox.Location = new System.Drawing.Point(160, 69);
            this.NameTeacherComboBox.Name = "NameTeacherComboBox";
            this.NameTeacherComboBox.Size = new System.Drawing.Size(279, 21);
            this.NameTeacherComboBox.TabIndex = 9;
            // 
            // NameTeacherLabel
            // 
            this.NameTeacherLabel.AutoSize = true;
            this.NameTeacherLabel.Location = new System.Drawing.Point(25, 69);
            this.NameTeacherLabel.Name = "NameTeacherLabel";
            this.NameTeacherLabel.Size = new System.Drawing.Size(116, 13);
            this.NameTeacherLabel.TabIndex = 10;
            this.NameTeacherLabel.Text = "ФИО Преподавателя";
            // 
            // AddFactButton
            // 
            this.AddFactButton.Location = new System.Drawing.Point(396, 381);
            this.AddFactButton.Name = "AddFactButton";
            this.AddFactButton.Size = new System.Drawing.Size(75, 23);
            this.AddFactButton.TabIndex = 10;
            this.AddFactButton.Text = "Добавить";
            this.AddFactButton.UseVisualStyleBackColor = true;
            // 
            // NameStudentComboBox
            // 
            this.NameStudentComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.NameStudentComboBox.FormattingEnabled = true;
            this.NameStudentComboBox.Location = new System.Drawing.Point(160, 33);
            this.NameStudentComboBox.Name = "NameStudentComboBox";
            this.NameStudentComboBox.Size = new System.Drawing.Size(279, 21);
            this.NameStudentComboBox.TabIndex = 10;
            // 
            // UniversityExamsFactAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 416);
            this.Controls.Add(this.AddFactButton);
            this.Controls.Add(this.SubjectGroupBox);
            this.Controls.Add(this.StudentGroupBox);
            this.Controls.Add(this.AddDateTimePicker);
            this.Controls.Add(this.DataLabel);
            this.Name = "UniversityExamsFactAdditionForm";
            this.Text = "Добавление факта учета оценок";
            this.StudentGroupBox.ResumeLayout(false);
            this.StudentGroupBox.PerformLayout();
            this.SubjectGroupBox.ResumeLayout(false);
            this.SubjectGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DataLabel;
        private System.Windows.Forms.DateTimePicker AddDateTimePicker;
        private System.Windows.Forms.Label StudentLabel;
        private System.Windows.Forms.ComboBox GroupComboBox;
        private System.Windows.Forms.GroupBox StudentGroupBox;
        private System.Windows.Forms.ComboBox NameStudentComboBox;
        private System.Windows.Forms.Label MarkLabel;
        private System.Windows.Forms.Label GroupLabel;
        private System.Windows.Forms.TextBox AddMarkТextBox;
        private System.Windows.Forms.ComboBox NameSubjectСomboBox;
        private System.Windows.Forms.Label NameSubjectLabel;
        private System.Windows.Forms.GroupBox SubjectGroupBox;
        private System.Windows.Forms.Label NameTeacherLabel;
        private System.Windows.Forms.ComboBox NameTeacherComboBox;
        private System.Windows.Forms.Button AddFactButton;
    }
}
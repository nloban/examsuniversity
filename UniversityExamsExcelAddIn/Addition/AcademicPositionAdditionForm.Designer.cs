﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class AcademicPositionAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AcademicPositionLabel = new System.Windows.Forms.Label();
            this.AcademicPositionTextBox = new System.Windows.Forms.TextBox();
            this.AddAcademicPositionButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AcademicPositionLabel
            // 
            this.AcademicPositionLabel.AutoSize = true;
            this.AcademicPositionLabel.Location = new System.Drawing.Point(13, 34);
            this.AcademicPositionLabel.Name = "AcademicPositionLabel";
            this.AcademicPositionLabel.Size = new System.Drawing.Size(197, 13);
            this.AcademicPositionLabel.TabIndex = 0;
            this.AcademicPositionLabel.Text = "Название академической должности";
            // 
            // AcademicPositionTextBox
            // 
            this.AcademicPositionTextBox.Location = new System.Drawing.Point(216, 31);
            this.AcademicPositionTextBox.Name = "AcademicPositionTextBox";
            this.AcademicPositionTextBox.Size = new System.Drawing.Size(298, 20);
            this.AcademicPositionTextBox.TabIndex = 1;
            // 
            // AddAcademicPositionButton
            // 
            this.AddAcademicPositionButton.Location = new System.Drawing.Point(439, 66);
            this.AddAcademicPositionButton.Name = "AddAcademicPositionButton";
            this.AddAcademicPositionButton.Size = new System.Drawing.Size(75, 23);
            this.AddAcademicPositionButton.TabIndex = 2;
            this.AddAcademicPositionButton.Text = "Добавить";
            this.AddAcademicPositionButton.UseVisualStyleBackColor = true;
            this.AddAcademicPositionButton.Click += new System.EventHandler(this.AddAcademicPositionButton_Click);
            // 
            // AcademicPositionAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(524, 101);
            this.Controls.Add(this.AddAcademicPositionButton);
            this.Controls.Add(this.AcademicPositionTextBox);
            this.Controls.Add(this.AcademicPositionLabel);
            this.Name = "AcademicPositionAdditionForm";
            this.Text = "Добавление академаческой должности";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label AcademicPositionLabel;
        private System.Windows.Forms.TextBox AcademicPositionTextBox;
        private System.Windows.Forms.Button AddAcademicPositionButton;
    }
}
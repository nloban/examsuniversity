﻿namespace UniversityExamsExcelAddIn.Addition
{
    partial class SubjectAdditionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SubjectNameLabel = new System.Windows.Forms.Label();
            this.SubjectNameTextBox = new System.Windows.Forms.TextBox();
            this.AddSubjectButton = new System.Windows.Forms.Button();
            this.ShortNameLabel = new System.Windows.Forms.Label();
            this.ShortSubjectNameTextBox = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // SubjectNameLabel
            // 
            this.SubjectNameLabel.AutoSize = true;
            this.SubjectNameLabel.Location = new System.Drawing.Point(12, 54);
            this.SubjectNameLabel.Name = "SubjectNameLabel";
            this.SubjectNameLabel.Size = new System.Drawing.Size(161, 13);
            this.SubjectNameLabel.TabIndex = 0;
            this.SubjectNameLabel.Text = "Полное название дисциплины";
            // 
            // SubjectNameTextBox
            // 
            this.SubjectNameTextBox.Location = new System.Drawing.Point(184, 51);
            this.SubjectNameTextBox.Name = "SubjectNameTextBox";
            this.SubjectNameTextBox.Size = new System.Drawing.Size(419, 20);
            this.SubjectNameTextBox.TabIndex = 1;
            // 
            // AddSubjectButton
            // 
            this.AddSubjectButton.Location = new System.Drawing.Point(528, 77);
            this.AddSubjectButton.Name = "AddSubjectButton";
            this.AddSubjectButton.Size = new System.Drawing.Size(75, 23);
            this.AddSubjectButton.TabIndex = 2;
            this.AddSubjectButton.Text = "Добавить";
            this.AddSubjectButton.UseVisualStyleBackColor = true;
            this.AddSubjectButton.Click += new System.EventHandler(this.AddSubjectButton_Click);
            // 
            // ShortNameLabel
            // 
            this.ShortNameLabel.AutoSize = true;
            this.ShortNameLabel.Location = new System.Drawing.Point(12, 19);
            this.ShortNameLabel.Name = "ShortNameLabel";
            this.ShortNameLabel.Size = new System.Drawing.Size(165, 13);
            this.ShortNameLabel.TabIndex = 3;
            this.ShortNameLabel.Text = "Краткое название дисциплины";
            // 
            // ShortSubjectNameTextBox
            // 
            this.ShortSubjectNameTextBox.Location = new System.Drawing.Point(184, 19);
            this.ShortSubjectNameTextBox.Name = "ShortSubjectNameTextBox";
            this.ShortSubjectNameTextBox.Size = new System.Drawing.Size(419, 20);
            this.ShortSubjectNameTextBox.TabIndex = 4;
            // 
            // SubjectAdditionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 112);
            this.Controls.Add(this.ShortSubjectNameTextBox);
            this.Controls.Add(this.ShortNameLabel);
            this.Controls.Add(this.AddSubjectButton);
            this.Controls.Add(this.SubjectNameTextBox);
            this.Controls.Add(this.SubjectNameLabel);
            this.Name = "SubjectAdditionForm";
            this.Text = "Добавление дисциплины";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label SubjectNameLabel;
        private System.Windows.Forms.TextBox SubjectNameTextBox;
        private System.Windows.Forms.Button AddSubjectButton;
        private System.Windows.Forms.Label ShortNameLabel;
        private System.Windows.Forms.TextBox ShortSubjectNameTextBox;
    }
}
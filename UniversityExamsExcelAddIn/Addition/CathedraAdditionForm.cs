﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using Telerik.OpenAccess.Exceptions;

namespace UniversityExamsExcelAddIn.Addition
{
    public partial class CathedraAdditionForm : Form
    {
        private readonly DimensionService dimensionService = new DimensionService();

        public CathedraAdditionForm()
        {
            InitializeComponent();
        }

        private void AddCathedraButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(ShortNameTextBox.Text) 
                && StringValueVerificationService.IsStringValueCorrect(CathedraTextBox.Text))
            {
                try
                {
                    AddCathedraButton.Enabled = false;
                    dimensionService.AddCathedra(ShortNameTextBox.Text, CathedraTextBox.Text); 
                    Close();
                }
                catch (DuplicateKeyException exception)
                {
                    MessageBox.Show("Данная кафедра уже существует!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    AddCathedraButton.Enabled = true;
                }
            }
        }
    }
}

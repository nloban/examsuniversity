-- ArtushevskayaUniversityExamsDataAccess.AcademicPosition
CREATE TABLE [AcademicPosition] (
    [AcademicPositionID] varchar(255) NOT NULL, -- _academicPositionID
    CONSTRAINT [pk_AcademicPosition] PRIMARY KEY ([AcademicPositionID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Cathedra
CREATE TABLE [Cathedra] (
    [NameCathedra] varchar(255) NULL,       -- _nameCathedra
    [CathedraID] varchar(255) NOT NULL,     -- _cathedraID
    CONSTRAINT [pk_Cathedra] PRIMARY KEY ([CathedraID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.DateDimension
CREATE TABLE [DateDimension] (
    [DateID] datetime NOT NULL,             -- _dateID
    CONSTRAINT [pk_DateDimension] PRIMARY KEY ([DateID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Faculty
CREATE TABLE [Faculty] (
    [NameFaculty] varchar(255) NULL,        -- _nameFaculty
    [FacultyID] varchar(255) NOT NULL,      -- _facultyID
    CONSTRAINT [pk_Faculty] PRIMARY KEY ([FacultyID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.GroupOfStudents
CREATE TABLE [GroupOfStudents] (
    [SpecialtyID] varchar(255) NOT NULL,    -- _specialtyID
    [NumberGroupID] varchar(255) NOT NULL,  -- _numberGroupID
    [FacultyID] varchar(255) NOT NULL,      -- _facultyID
    CONSTRAINT [pk_GroupOfStudents] PRIMARY KEY ([FacultyID], [NumberGroupID], [SpecialtyID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.ScientificDegree
CREATE TABLE [ScientificDegree] (
    [ScientificDegreeID] varchar(255) NOT NULL, -- _scientificDegreeID
    CONSTRAINT [pk_ScientificDegree] PRIMARY KEY ([ScientificDegreeID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Speciality
CREATE TABLE [Speciality] (
    [SpecialtyID] varchar(255) NOT NULL,    -- _specialtyID
    [NameSpecialty] varchar(255) NULL,      -- _nameSpecialty
    [FacultyID] varchar(255) NOT NULL,      -- _facultyID
    CONSTRAINT [pk_Speciality] PRIMARY KEY ([FacultyID], [SpecialtyID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Student
CREATE TABLE [Student] (
    [StudentID] int IDENTITY NOT NULL,      -- _studentID
    [SexStudent] varchar(255) NULL,         -- _sexStudent
    [NameStudent] varchar(255) NULL,        -- _nameStudent
    [FacultyID] varchar(255) NULL,          -- _groupOfStudents
    [NumberGroupID] varchar(255) NULL,      -- _groupOfStudents
    [SpecialtyID] varchar(255) NULL,        -- _groupOfStudents
    CONSTRAINT [pk_Student] PRIMARY KEY ([StudentID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Subject
CREATE TABLE [Subject] (
    [SubjectID] varchar(255) NOT NULL,      -- _subjectID
    [NameSubject] varchar(255) NULL,        -- _nameSubject
    CONSTRAINT [pk_Subject] PRIMARY KEY ([SubjectID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.Teacher
CREATE TABLE [Teacher] (
    [TeacherID] int IDENTITY NOT NULL,      -- _teacherID
    [ScientificDegreeID] varchar(255) NULL, -- _scientificDegree
    [NameTeacher] varchar(255) NULL,        -- _nameTeacher
    [CathedraID] varchar(255) NULL,         -- _cathedra
    [AcademicPositionID] varchar(255) NULL, -- _academicPosition
    CONSTRAINT [pk_Teacher] PRIMARY KEY ([TeacherID])
)
go

-- ArtushevskayaUniversityExamsDataAccess.UniversityExamsFact
CREATE TABLE [UniversityExamsFact] (
    [TeacherID] int NOT NULL,               -- _teacherID
    [SubjectID] varchar(255) NOT NULL,      -- _subjectID
    [StudentID] int NOT NULL,               -- _studentID
    [Semester] int NOT NULL,                -- _semester
    [Mark] int NOT NULL,                    -- _mark
    [DateID] datetime NOT NULL,             -- _dateID
    CONSTRAINT [pk_UniversityExamsFact] PRIMARY KEY ([DateID], [StudentID], [SubjectID], [TeacherID])
)
go

CREATE INDEX [idx_GrpOfStdnts_FcltyID_Spclty] ON [GroupOfStudents]([FacultyID], [SpecialtyID])
go

CREATE INDEX [idx_Speciality_FacultyID] ON [Speciality]([FacultyID])
go

CREATE INDEX [idx_Stdnt_FcltyID_NmbrGrpID_Sp] ON [Student]([FacultyID], [NumberGroupID], [SpecialtyID])
go

CREATE INDEX [idx_Teacher_ScientificDegreeID] ON [Teacher]([ScientificDegreeID])
go

CREATE INDEX [idx_Teacher_CathedraID] ON [Teacher]([CathedraID])
go

CREATE INDEX [idx_Teacher_AcademicPositionID] ON [Teacher]([AcademicPositionID])
go

CREATE INDEX [idx_UnvrstyExamsFact_TeacherID] ON [UniversityExamsFact]([TeacherID])
go

CREATE INDEX [idx_UnvrstyExamsFact_SubjectID] ON [UniversityExamsFact]([SubjectID])
go

CREATE INDEX [idx_UnvrstyExamsFact_StudentID] ON [UniversityExamsFact]([StudentID])
go

CREATE INDEX [idx_UniversityExamsFact_DateID] ON [UniversityExamsFact]([DateID])
go

ALTER TABLE [GroupOfStudents] ADD CONSTRAINT [ref_GroupOfStudents_Speciality] FOREIGN KEY ([FacultyID], [SpecialtyID]) REFERENCES [Speciality]([FacultyID], [SpecialtyID])
go

ALTER TABLE [Speciality] ADD CONSTRAINT [ref_Speciality_Faculty] FOREIGN KEY ([FacultyID]) REFERENCES [Faculty]([FacultyID])
go

ALTER TABLE [Student] ADD CONSTRAINT [ref_Student_GroupOfStudents] FOREIGN KEY ([FacultyID], [NumberGroupID], [SpecialtyID]) REFERENCES [GroupOfStudents]([FacultyID], [NumberGroupID], [SpecialtyID])
go

ALTER TABLE [Teacher] ADD CONSTRAINT [ref_Teacher_AcademicPosition] FOREIGN KEY ([AcademicPositionID]) REFERENCES [AcademicPosition]([AcademicPositionID])
go

ALTER TABLE [Teacher] ADD CONSTRAINT [ref_Teacher_Cathedra] FOREIGN KEY ([CathedraID]) REFERENCES [Cathedra]([CathedraID])
go

ALTER TABLE [Teacher] ADD CONSTRAINT [ref_Teacher_ScientificDegree] FOREIGN KEY ([ScientificDegreeID]) REFERENCES [ScientificDegree]([ScientificDegreeID])
go

ALTER TABLE [UniversityExamsFact] ADD CONSTRAINT [ref_UnvrstyExmsFct_DtDimension] FOREIGN KEY ([DateID]) REFERENCES [DateDimension]([DateID])
go

ALTER TABLE [UniversityExamsFact] ADD CONSTRAINT [ref_UnversityExamsFact_Student] FOREIGN KEY ([StudentID]) REFERENCES [Student]([StudentID])
go

ALTER TABLE [UniversityExamsFact] ADD CONSTRAINT [ref_UnversityExamsFact_Subject] FOREIGN KEY ([SubjectID]) REFERENCES [Subject]([SubjectID])
go

ALTER TABLE [UniversityExamsFact] ADD CONSTRAINT [ref_UnversityExamsFact_Teacher] FOREIGN KEY ([TeacherID]) REFERENCES [Teacher]([TeacherID])
go


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;
using Microsoft.Office.Interop.Excel;

namespace UniversityExamsExcelAddIn
{
    public partial class SecondSliceForm : Form
    {

        private readonly DimensionService dimensionService = new DimensionService();
        private Int32 minYear;
        private Int32 maxYear;

        private List<ScientificDegree> scientificDegrees;
        public SecondSliceForm()
        {
            InitializeComponent();
        }

        private void ExecuteButton_Click(object sender, EventArgs e)
        {
            if (StartYearComboBox.SelectedItem != null && EndYearComboBox.SelectedItem != null)
            {
                ExecuteButton.Enabled = false;
                Globals.ThisAddIn.Application.Cells.Clear();

                var facts =
                    dimensionService.GetFactsByYears(Int32.Parse(StartYearComboBox.SelectedItem.ToString()),
                        Int32.Parse(EndYearComboBox.SelectedItem.ToString()));
                ShowSlice(facts);
                Close();
            }
        }

        private void StartYearComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            var selectedYear = Int32.Parse(StartYearComboBox.SelectedItem.ToString());
            if (selectedYear == maxYear)
            {
                EndYearComboBox.SelectedItem = maxYear;
            }
            else
            {
                EndYearComboBox.Items.Clear();
                for (int i = selectedYear - minYear; i <= maxYear - minYear; i++)
                {
                    EndYearComboBox.Items.Insert(i - (selectedYear - minYear), minYear + i);
                }
            }
        }

        private void EndYearComboBox_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (Int32.Parse(EndYearComboBox.SelectedItem.ToString()) == minYear)
            {
                StartYearComboBox.SelectedItem = minYear;
            }
            else
            {
                if (StartYearComboBox.SelectedItem == null ||
                    Int32.Parse(StartYearComboBox.SelectedItem.ToString()) >
                    Int32.Parse(EndYearComboBox.SelectedItem.ToString()))
                {
                    StartYearComboBox.Items.Clear();
                    for (int i = 0; i <= (Int32.Parse(EndYearComboBox.SelectedItem.ToString()) - minYear); i++)
                    {
                        StartYearComboBox.Items.Insert(i, minYear + i);
                    }
                }
            }
        }

        private void SecondSliceForm_Load(object sender, EventArgs e)
        {
            Globals.ThisAddIn.Application.Cells.Clear();
            minYear = dimensionService.GetMinDate().Year;
            maxYear = dimensionService.GetMaxDate().Year;
            scientificDegrees = dimensionService.GetAllScientificDegrees().OrderBy(type => type.ScientificDegreeID).ToList();

            for (int i = 0; i <= (maxYear - minYear); i++)
            {
                StartYearComboBox.Items.Insert(i, minYear + i);
                EndYearComboBox.Items.Insert(i, minYear + i);
            }
        }

        private void ShowSlice(Dictionary<Int32, List<UniversityExamsFact>> dictionary)
        {
            InitializeHeaders(dictionary);
            InitializeData(dictionary);
        }

        private void InitializeData(Dictionary<Int32, List<UniversityExamsFact>> dictionary)
        {
            int width = 2 + scientificDegrees.Count;
            int height = 1;
            int i = 2;
            foreach (var scientificDegree in scientificDegrees)
            {
                Double commonSumByEngineType = 0;
                int j = 2;
                foreach (var item in dictionary)
                {
                    var facts = item.Value.Where(fact => fact.Teacher.ScientificDegreeID.Equals(scientificDegree.ScientificDegreeID));
                    var marks = facts.Select(fact => fact.Mark).ToList();
                    var avg = marks.Count() != 0 ? marks.Average() : 0;
                    Globals.ThisAddIn.Application.Cells[j, 2 + scientificDegrees.Count] = Math.Round((decimal)item.Value.Select(fact => fact.Mark).Average(), 2);
                    Globals.ThisAddIn.Application.Cells[j, i] = Math.Round((decimal)avg, 2);
                    commonSumByEngineType += avg;
                    j++;
                }

                var commonMarks =
                    dictionary.Select(
                        facts =>
                            facts.Value.Where(
                                fact => fact.Teacher.ScientificDegreeID.Equals(scientificDegree.ScientificDegreeID))
                                .Select(fact => fact.Mark).DefaultIfEmpty().Average()).ToList();
                var commonAvg = commonMarks.Count() != 0 ? commonMarks.Average() : 0;
                Globals.ThisAddIn.Application.Cells[j, i] = Math.Round((decimal)commonAvg, 2);
                Globals.ThisAddIn.Application.Cells[j, i].Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();
                height = j;
                i++;
            }

            Globals.ThisAddIn.Application.Cells[height, width] = Math.Round((decimal)dictionary.Select(facts => facts.Value.Select(fact => fact.Mark).Average()).Average(), 2);
            Globals.ThisAddIn.Application.Cells[height, width].Interior.Color = System.Drawing.Color.MediumSpringGreen.ToArgb();

            Globals.ThisAddIn.Application.Range["A1", GetExcelColumnName(width, height)].Columns.AutoFit();

        }

        private void InitializeHeaders(Dictionary<Int32, List<UniversityExamsFact>> dictionary)
        {
            int width = 2 + scientificDegrees.Count;
            int height = 1;
            int i = 2;
            foreach (var scientificDegree in scientificDegrees)
            {
                Globals.ThisAddIn.Application.Cells[1, i] = scientificDegree.ScientificDegreeID;
                Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.LightSkyBlue.ToArgb();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[1, i] = "Всего за период";
            Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.Orchid.ToArgb();

            int j = 2;
            foreach (var item in dictionary)
            {
                Globals.ThisAddIn.Application.Cells[j, 1] = item.Key;
                Globals.ThisAddIn.Application.Cells[j, 1].HorizontalAlignment = Constants.xlLeft;
                Globals.ThisAddIn.Application.Cells[j, 1].Interior.Color = System.Drawing.Color.GreenYellow.ToArgb();

                j++;
            }
            height = j;
            Globals.ThisAddIn.Application.Cells[j, 1] = "Всего";
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, j), GetExcelColumnName(2, j)].Cells.Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();
        }

        private string GetExcelColumnName(int columnNumber, int rowNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return String.Format("{0}{1}", columnName, rowNumber);
        }
    }
}

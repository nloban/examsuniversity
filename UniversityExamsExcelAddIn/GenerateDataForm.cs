﻿using System;
using System.Windows.Forms;

namespace UniversityExamsExcelAddIn
{
    public partial class GenerateDataForm : Form
    {
        public GenerateDataForm()
        {
            InitializeComponent();
        }

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            if (AmountOFItemsComboBox.SelectedItem != null && StartDateTimePicker.Value < EndDateTimePicker.Value)
            {
                GenerateButton.Enabled = false;
                ServiceReference.WcfServiceClient client = new ServiceReference.WcfServiceClient();
                client.GenerateFacts(StartDateTimePicker.Value, EndDateTimePicker.Value, UInt32.Parse(AmountOFItemsComboBox.SelectedItem.ToString()));
                Close();
            }
            else if (AmountOFItemsComboBox.SelectedItem == null)
            {
                MessageBox.Show("Выберите количество генерируемых фактов", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (StartDateTimePicker.Value >= EndDateTimePicker.Value)
            {
                MessageBox.Show("Конец периода должен быть больше начала", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void GenerateDataForm_Load(object sender, EventArgs e)
        {
            AmountOFItemsComboBox.Items.AddRange(new object[] { 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000 });
            StartDateTimePicker.MaxDate = DateTime.Today.AddDays(-1);
            EndDateTimePicker.MaxDate = DateTime.Today;
        }
    }
}

﻿namespace UniversityExamsExcelAddIn
{
    partial class GenerateDataForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.GenerateButton = new System.Windows.Forms.Button();
            this.AmountLabel = new System.Windows.Forms.Label();
            this.StartDemoLabel = new System.Windows.Forms.Label();
            this.EndDateLabel = new System.Windows.Forms.Label();
            this.AmountOFItemsComboBox = new System.Windows.Forms.ComboBox();
            this.StartDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.EndDateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.SuspendLayout();
            // 
            // GenerateButton
            // 
            this.GenerateButton.Location = new System.Drawing.Point(130, 226);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(117, 23);
            this.GenerateButton.TabIndex = 0;
            this.GenerateButton.Text = "Сгенерировать";
            this.GenerateButton.UseVisualStyleBackColor = true;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // AmountLabel
            // 
            this.AmountLabel.AutoSize = true;
            this.AmountLabel.Location = new System.Drawing.Point(12, 37);
            this.AmountLabel.Name = "AmountLabel";
            this.AmountLabel.Size = new System.Drawing.Size(111, 13);
            this.AmountLabel.TabIndex = 1;
            this.AmountLabel.Text = "Количество записей";
            // 
            // StartDemoLabel
            // 
            this.StartDemoLabel.AutoSize = true;
            this.StartDemoLabel.Location = new System.Drawing.Point(12, 87);
            this.StartDemoLabel.Name = "StartDemoLabel";
            this.StartDemoLabel.Size = new System.Drawing.Size(89, 13);
            this.StartDemoLabel.TabIndex = 2;
            this.StartDemoLabel.Text = "Начало периода";
            // 
            // EndDateLabel
            // 
            this.EndDateLabel.AutoSize = true;
            this.EndDateLabel.Location = new System.Drawing.Point(12, 149);
            this.EndDateLabel.Name = "EndDateLabel";
            this.EndDateLabel.Size = new System.Drawing.Size(83, 13);
            this.EndDateLabel.TabIndex = 3;
            this.EndDateLabel.Text = "Конец периода";
            // 
            // AmountOFItemsComboBox
            // 
            this.AmountOFItemsComboBox.FormattingEnabled = true;
            this.AmountOFItemsComboBox.Location = new System.Drawing.Point(161, 37);
            this.AmountOFItemsComboBox.Name = "AmountOFItemsComboBox";
            this.AmountOFItemsComboBox.Size = new System.Drawing.Size(221, 21);
            this.AmountOFItemsComboBox.TabIndex = 4;
            // 
            // StartDateTimePicker
            // 
            this.StartDateTimePicker.Location = new System.Drawing.Point(161, 87);
            this.StartDateTimePicker.Name = "StartDateTimePicker";
            this.StartDateTimePicker.Size = new System.Drawing.Size(221, 20);
            this.StartDateTimePicker.TabIndex = 5;
            // 
            // EndDateTimePicker
            // 
            this.EndDateTimePicker.Location = new System.Drawing.Point(161, 143);
            this.EndDateTimePicker.Name = "EndDateTimePicker";
            this.EndDateTimePicker.Size = new System.Drawing.Size(221, 20);
            this.EndDateTimePicker.TabIndex = 6;
            // 
            // GenerateDataForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(394, 269);
            this.Controls.Add(this.EndDateTimePicker);
            this.Controls.Add(this.StartDateTimePicker);
            this.Controls.Add(this.AmountOFItemsComboBox);
            this.Controls.Add(this.EndDateLabel);
            this.Controls.Add(this.StartDemoLabel);
            this.Controls.Add(this.AmountLabel);
            this.Controls.Add(this.GenerateButton);
            this.Name = "GenerateDataForm";
            this.Text = "Генерация данных";
            this.Load += new System.EventHandler(this.GenerateDataForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.Label AmountLabel;
        private System.Windows.Forms.Label StartDemoLabel;
        private System.Windows.Forms.Label EndDateLabel;
        private System.Windows.Forms.ComboBox AmountOFItemsComboBox;
        private System.Windows.Forms.DateTimePicker StartDateTimePicker;
        private System.Windows.Forms.DateTimePicker EndDateTimePicker;
    }
}
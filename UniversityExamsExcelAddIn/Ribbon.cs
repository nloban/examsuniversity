﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ArtushevskayaUniversityExamsDataAccess;
using ArtushevskayaUniversityExamsServices;
using Microsoft.Office.Interop.Excel;
using Microsoft.Office.Tools.Ribbon;
using UniversityExamsExcelAddIn.Addition;

namespace UniversityExamsExcelAddIn
{
    public partial class Ribbon
    {
        private readonly DimensionService dimensionService = new DimensionService();

        private List<Faculty> faculties;
 
        private void Ribbon_Load(object sender, RibbonUIEventArgs e)
        {

        }

        private void GenerateDataButton_Click(object sender, RibbonControlEventArgs e)
        {
            new GenerateDataForm().Show();
        }

        private void AddFacultyButton_Click(object sender, RibbonControlEventArgs e)
        {
            new FacultyAdditionForm().Show();
        }

        private void AddGroupButton_Click(object sender, RibbonControlEventArgs e)
        {
            new GroupAdditionForm().Show();
        }

        private void AddSpecialtyButton_Click(object sender, RibbonControlEventArgs e)
        {
            new SpecialtyAdditionForm().Show();
        }

        private void AddStudentВutton_Click(object sender, RibbonControlEventArgs e)
        {
            new StudentAdditionForm().Show();
        }

        private void AddTeacherButton_Click(object sender, RibbonControlEventArgs e)
        {
            new TeacherAdditionForm().Show();
        }

        private void AddCathedraButton_Click(object sender, RibbonControlEventArgs e)
        {
            new CathedraAdditionForm().Show();
        }

        private void AddDegreeButton_Click(object sender, RibbonControlEventArgs e)
        {
            new ScientificDegreeAdditionForm().Show();
        }

        private void AddPositionButton_Click(object sender, RibbonControlEventArgs e)
        {
            new AcademicPositionAdditionForm().Show();
        }
        
        private void AddSubjectButton_Click(object sender, RibbonControlEventArgs e)
        {
            new SubjectAdditionForm().Show();
        }

        private void ExecuteQueryButton_Click(object sender, RibbonControlEventArgs e)
        {
            new QueryExecutionForm().Show();
        }

        private void GraphButton_Click(object sender, RibbonControlEventArgs e)
        {
            var minDate = dimensionService.GetMinDate();
            var maxDate = dimensionService.GetMaxDate();

            CreateChart(dimensionService.GetDataForChart(minDate, maxDate));
        }

        private void CreateChart(Dictionary<Int32, Double> chartValues)
        {
            Worksheet worksheet = (Worksheet)Globals.ThisAddIn.Application.Sheets[1];

            var chartObjects = (ChartObjects)worksheet.ChartObjects();
            ChartObject chartObject = chartObjects.Add(200, 20, 500, 300);
            Chart excelChart = chartObject.Chart;
            excelChart.HasTitle = true;
            excelChart.ChartTitle.Text = "Средняя успеваемость студентов по годам";

            excelChart.ChartType = XlChartType.xlLineStacked;

            excelChart.Axes(XlAxisType.xlCategory).HasTitle = true;
            excelChart.Axes(XlAxisType.xlCategory).AxisTitle.Text = "Год";
            excelChart.Axes(XlAxisType.xlValue).HasTitle = true;
            excelChart.Axes(XlAxisType.xlValue).AxisTitle.Text = "Средний балл";
            worksheet.Cells[1, 2] = "Год";
            worksheet.Cells[1, 3] = "Средний балл";

            int i = 0;
            foreach (var chartValue in chartValues)
            {
                worksheet.Cells[i + 2, 2] = chartValue.Key;
                worksheet.Cells[i + 2, 3] = Math.Round((decimal)chartValue.Value, 2);
                i++;
            }

            WorksheetFunction wsf = Globals.ThisAddIn.Application.WorksheetFunction;
            double corel = wsf.Correl(chartValues.Select(d => d.Key).ToArray(), chartValues.Select(d => d.Value).ToArray());
            double stDev1 = wsf.StDev(chartValues.Select(d => d.Value).ToArray());
            double stDev2 = wsf.StDev(chartValues.Select(d => d.Key).ToArray());
            double stDev = stDev1 / stDev2;
            double cofb = corel * stDev;
            double math1 = chartValues.Select(d => d.Value).Average();
            double math2 = (double)chartValues.Select(d => d.Key).Average();
            double kofa = math1 - cofb * math2;
            double[] massY = new double[3];
            int[] massX = new int[3];
            int av = 0;
            for (int g = chartValues.Select(k => k.Key).Last(); g < chartValues.Select(k => k.Key).Last() + 3; g++)
            {
                massX[av] = g;
                av++;
            }
            int w = 0;
            for (int j3 = chartValues.Count; j3 < chartValues.Count + 3; j3++)
            {
                massY[w] = Convert.ToDouble(kofa + cofb * massX[w]);
                w++;
            }

            int h = 1;
            for (int i1 = chartValues.Count; i1 < chartValues.Count + 3; i1++)
            {
                Globals.ThisAddIn.Application.Cells[i1 + 2, 2] = chartValues.Select(d => d.Key).ToArray()[chartValues.Count - 1] + h;
                Globals.ThisAddIn.Application.Cells[i1 + 2, 3] = Math.Round((decimal)massY[h - 1], 2);
                h++;
            }

            worksheet.Range["B1", "C1"].Cells.Interior.Color = System.Drawing.Color.DarkSalmon.ToArgb();

            worksheet.Range["B1", "B" + chartValues.Count].Columns.AutoFit();
            worksheet.Range["C1", "C" + chartValues.Count].Columns.AutoFit();

            Range xValues = worksheet.Range["B2", "B" + (chartValues.Count + 3)];
            Range values = worksheet.Range["C2", "C" + (chartValues.Count + 3)];
            SeriesCollection seriesCollection = excelChart.SeriesCollection();

            Series series = seriesCollection.NewSeries();
            series.XValues = xValues;
            series.Values = values;
            series.Name = "Средний балл";

            Globals.ThisAddIn.Application.Visible = true;
        }

        private void FacultyAndSexSliceButton_Click(object sender, RibbonControlEventArgs e)
        {
            var facts = dimensionService.GetDataForSliceForFacultyAndSexOfStudents();
            faculties = dimensionService.GetAllFaculties();
            ShowSlice(facts);
        }

        private void ShowSlice(Dictionary<String, Dictionary<String, Int32>> dictionary)
        {
            Globals.ThisAddIn.Application.Cells.Clear();
            InitializeHeaders(dictionary);
            InitializeData(dictionary);
        }

        private void InitializeData(Dictionary<String, Dictionary<String, Int32>> dictionary)
        {
            int height = dictionary["Мужской"].Count;
            int width = 2;

            int i = 2;
            foreach (var item in dictionary)
            {
                int j = 2;
                foreach (var faculty in item.Value)
                {
                    var newListDictionary = dictionary.Select(x => x.Value).ToList();
                    Globals.ThisAddIn.Application.Cells[j, i] = faculty.Value;
                    Globals.ThisAddIn.Application.Cells[j, width + 2] = newListDictionary.Select(x => x.Where(k => k.Key.Equals(faculty.Key)).Select(y => y.Value).Sum()).Sum();
                    j++;
                }
                Globals.ThisAddIn.Application.Cells[j, i] = item.Value.Select(x => x.Value).Sum();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[height + 2, width + 2] = dictionary.Select(fact => fact.Value.Select(k => k.Value).Sum()).Sum();
            Globals.ThisAddIn.Application.Cells[height + 2, width + 2].Interior.Color = System.Drawing.Color.LightCoral.ToArgb();
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(width + 2, 2), GetExcelColumnName(width + 2, height + 1)].Interior.Color = System.Drawing.Color.BlueViolet.ToArgb();
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 1), GetExcelColumnName(width + 2, height + 2)].Columns.AutoFit();
        }

        private void InitializeHeaders(Dictionary<String, Dictionary<String, Int32>> dictionary)
        {
            int i = 2;
            foreach (var item in dictionary)
            {
                Globals.ThisAddIn.Application.Cells[1, i] = item.Key;
                Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.Orange.ToArgb();
                i++;
            }

            Globals.ThisAddIn.Application.Cells[1, i] = "Всего";
            Globals.ThisAddIn.Application.Cells[1, i].Interior.Color = System.Drawing.Color.MediumTurquoise.ToArgb();

            int j = 2;
            foreach (var faculty in dictionary["Мужской"].Keys)
            {
                Globals.ThisAddIn.Application.Cells[j, 1] = faculty;
                j++;
            }

            
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 2), GetExcelColumnName(1, j - 1)].Cells.Interior.Color = System.Drawing.Color.LimeGreen.ToArgb();
            
            Globals.ThisAddIn.Application.Cells[j, 1] = "Всего";
            Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, j), GetExcelColumnName(3, j)].Cells.Interior.Color = System.Drawing.Color.Orchid.ToArgb();

        }

        private string GetExcelColumnName(int columnNumber, int rowNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return String.Format("{0}{1}", columnName, rowNumber);
        }

        private void SecondSliceButton_Click(object sender, RibbonControlEventArgs e)
        {
            new SecondSliceForm().Show();
        }
    }
}

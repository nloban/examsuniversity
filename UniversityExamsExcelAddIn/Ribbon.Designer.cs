﻿namespace UniversityExamsExcelAddIn
{
    partial class Ribbon : Microsoft.Office.Tools.Ribbon.RibbonBase
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        public Ribbon()
            : base(Globals.Factory.GetRibbonFactory())
        {
            InitializeComponent();
        }

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.tab1 = this.Factory.CreateRibbonTab();
            this.group1 = this.Factory.CreateRibbonGroup();
            this.tab2 = this.Factory.CreateRibbonTab();
            this.DataGenerateGroup = this.Factory.CreateRibbonGroup();
            this.GenerateDataButton = this.Factory.CreateRibbonButton();
            this.DataAdditionGroup = this.Factory.CreateRibbonGroup();
            this.AddFacultyButton = this.Factory.CreateRibbonButton();
            this.AddGroupButton = this.Factory.CreateRibbonButton();
            this.AddSpecialtyButton = this.Factory.CreateRibbonButton();
            this.AddStudentВutton = this.Factory.CreateRibbonButton();
            this.separator1 = this.Factory.CreateRibbonSeparator();
            this.AddTeacherButton = this.Factory.CreateRibbonButton();
            this.AddCathedraButton = this.Factory.CreateRibbonButton();
            this.AddDegreeButton = this.Factory.CreateRibbonButton();
            this.AddPositionButton = this.Factory.CreateRibbonButton();
            this.AddSubjectButton = this.Factory.CreateRibbonButton();
            this.SqlQueryGroup = this.Factory.CreateRibbonGroup();
            this.ExecuteQueryButton = this.Factory.CreateRibbonButton();
            this.GraphGroup = this.Factory.CreateRibbonGroup();
            this.SGroup = this.Factory.CreateRibbonGroup();
            this.GraphButton = this.Factory.CreateRibbonButton();
            this.FacultyAndSexSliceButton = this.Factory.CreateRibbonButton();
            this.SecondSliceButton = this.Factory.CreateRibbonButton();
            this.tab1.SuspendLayout();
            this.tab2.SuspendLayout();
            this.DataGenerateGroup.SuspendLayout();
            this.DataAdditionGroup.SuspendLayout();
            this.SqlQueryGroup.SuspendLayout();
            this.GraphGroup.SuspendLayout();
            this.SGroup.SuspendLayout();
            // 
            // tab1
            // 
            this.tab1.ControlId.ControlIdType = Microsoft.Office.Tools.Ribbon.RibbonControlIdType.Office;
            this.tab1.Groups.Add(this.group1);
            this.tab1.Label = "TabAddIns";
            this.tab1.Name = "tab1";
            // 
            // group1
            // 
            this.group1.Label = "group1";
            this.group1.Name = "group1";
            // 
            // tab2
            // 
            this.tab2.Groups.Add(this.DataGenerateGroup);
            this.tab2.Groups.Add(this.DataAdditionGroup);
            this.tab2.Groups.Add(this.SqlQueryGroup);
            this.tab2.Groups.Add(this.GraphGroup);
            this.tab2.Groups.Add(this.SGroup);
            this.tab2.Label = "Артюшевская Л.С.";
            this.tab2.Name = "tab2";
            // 
            // DataGenerateGroup
            // 
            this.DataGenerateGroup.Items.Add(this.GenerateDataButton);
            this.DataGenerateGroup.Label = "Генерация данных";
            this.DataGenerateGroup.Name = "DataGenerateGroup";
            // 
            // GenerateDataButton
            // 
            this.GenerateDataButton.Label = "Сгенерировать данные";
            this.GenerateDataButton.Name = "GenerateDataButton";
            this.GenerateDataButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.GenerateDataButton_Click);
            // 
            // DataAdditionGroup
            // 
            this.DataAdditionGroup.Items.Add(this.AddFacultyButton);
            this.DataAdditionGroup.Items.Add(this.AddGroupButton);
            this.DataAdditionGroup.Items.Add(this.AddSpecialtyButton);
            this.DataAdditionGroup.Items.Add(this.AddStudentВutton);
            this.DataAdditionGroup.Items.Add(this.separator1);
            this.DataAdditionGroup.Items.Add(this.AddTeacherButton);
            this.DataAdditionGroup.Items.Add(this.AddCathedraButton);
            this.DataAdditionGroup.Items.Add(this.AddDegreeButton);
            this.DataAdditionGroup.Items.Add(this.AddPositionButton);
            this.DataAdditionGroup.Items.Add(this.AddSubjectButton);
            this.DataAdditionGroup.Label = "Добавление измерений";
            this.DataAdditionGroup.Name = "DataAdditionGroup";
            // 
            // AddFacultyButton
            // 
            this.AddFacultyButton.Label = "Факультет";
            this.AddFacultyButton.Name = "AddFacultyButton";
            this.AddFacultyButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddFacultyButton_Click);
            // 
            // AddGroupButton
            // 
            this.AddGroupButton.Label = "Группа";
            this.AddGroupButton.Name = "AddGroupButton";
            this.AddGroupButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddGroupButton_Click);
            // 
            // AddSpecialtyButton
            // 
            this.AddSpecialtyButton.Label = "Специальность";
            this.AddSpecialtyButton.Name = "AddSpecialtyButton";
            this.AddSpecialtyButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddSpecialtyButton_Click);
            // 
            // AddStudentВutton
            // 
            this.AddStudentВutton.Label = "Студент";
            this.AddStudentВutton.Name = "AddStudentВutton";
            this.AddStudentВutton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddStudentВutton_Click);
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            // 
            // AddTeacherButton
            // 
            this.AddTeacherButton.Label = "Преподаватель";
            this.AddTeacherButton.Name = "AddTeacherButton";
            this.AddTeacherButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddTeacherButton_Click);
            // 
            // AddCathedraButton
            // 
            this.AddCathedraButton.Label = "Кафедра";
            this.AddCathedraButton.Name = "AddCathedraButton";
            this.AddCathedraButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddCathedraButton_Click);
            // 
            // AddDegreeButton
            // 
            this.AddDegreeButton.Label = "Ученая степень";
            this.AddDegreeButton.Name = "AddDegreeButton";
            this.AddDegreeButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddDegreeButton_Click);
            // 
            // AddPositionButton
            // 
            this.AddPositionButton.Label = "Академическая должность";
            this.AddPositionButton.Name = "AddPositionButton";
            this.AddPositionButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddPositionButton_Click);
            // 
            // AddSubjectButton
            // 
            this.AddSubjectButton.Label = "Дисциплина";
            this.AddSubjectButton.Name = "AddSubjectButton";
            this.AddSubjectButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.AddSubjectButton_Click);
            // 
            // SqlQueryGroup
            // 
            this.SqlQueryGroup.Items.Add(this.ExecuteQueryButton);
            this.SqlQueryGroup.Label = "SQL запрос";
            this.SqlQueryGroup.Name = "SqlQueryGroup";
            // 
            // ExecuteQueryButton
            // 
            this.ExecuteQueryButton.Label = "Выполнить SQL запрос";
            this.ExecuteQueryButton.Name = "ExecuteQueryButton";
            this.ExecuteQueryButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.ExecuteQueryButton_Click);
            // 
            // GraphGroup
            // 
            this.GraphGroup.Items.Add(this.GraphButton);
            this.GraphGroup.Label = "Графики";
            this.GraphGroup.Name = "GraphGroup";
            // 
            // SGroup
            // 
            this.SGroup.Items.Add(this.FacultyAndSexSliceButton);
            this.SGroup.Items.Add(this.SecondSliceButton);
            this.SGroup.Label = "Срезы";
            this.SGroup.Name = "SGroup";
            // 
            // GraphButton
            // 
            this.GraphButton.Label = "График успеваемости по годам";
            this.GraphButton.Name = "GraphButton";
            this.GraphButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.GraphButton_Click);
            // 
            // FacultyAndSexSliceButton
            // 
            this.FacultyAndSexSliceButton.Label = "Гендерное соотношение студентов в зависимости от факультета";
            this.FacultyAndSexSliceButton.Name = "FacultyAndSexSliceButton";
            this.FacultyAndSexSliceButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.FacultyAndSexSliceButton_Click);
            // 
            // SecondSliceButton
            // 
            this.SecondSliceButton.Label = "Динамика успеваемости студентов в зависимости от ученого звания преподавателя";
            this.SecondSliceButton.Name = "SecondSliceButton";
            this.SecondSliceButton.Click += new Microsoft.Office.Tools.Ribbon.RibbonControlEventHandler(this.SecondSliceButton_Click);
            // 
            // Ribbon
            // 
            this.Name = "Ribbon";
            this.RibbonType = "Microsoft.Excel.Workbook";
            this.Tabs.Add(this.tab1);
            this.Tabs.Add(this.tab2);
            this.Load += new Microsoft.Office.Tools.Ribbon.RibbonUIEventHandler(this.Ribbon_Load);
            this.tab1.ResumeLayout(false);
            this.tab1.PerformLayout();
            this.tab2.ResumeLayout(false);
            this.tab2.PerformLayout();
            this.DataGenerateGroup.ResumeLayout(false);
            this.DataGenerateGroup.PerformLayout();
            this.DataAdditionGroup.ResumeLayout(false);
            this.DataAdditionGroup.PerformLayout();
            this.SqlQueryGroup.ResumeLayout(false);
            this.SqlQueryGroup.PerformLayout();
            this.GraphGroup.ResumeLayout(false);
            this.GraphGroup.PerformLayout();
            this.SGroup.ResumeLayout(false);
            this.SGroup.PerformLayout();

        }

        #endregion

        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab1;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup group1;
        internal Microsoft.Office.Tools.Ribbon.RibbonTab tab2;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup DataGenerateGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton GenerateDataButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup DataAdditionGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddStudentВutton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddGroupButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddSpecialtyButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddFacultyButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddTeacherButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddCathedraButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddDegreeButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonSeparator separator1;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddPositionButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton AddSubjectButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup SqlQueryGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton ExecuteQueryButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup GraphGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonGroup SGroup;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton GraphButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton FacultyAndSexSliceButton;
        internal Microsoft.Office.Tools.Ribbon.RibbonButton SecondSliceButton;
    }

    partial class ThisRibbonCollection
    {
        internal Ribbon Ribbon
        {
            get { return this.GetRibbon<Ribbon>(); }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ArtushevskayaUniversityExamsServices;
using UniversityExamsExcelAddIn.ServiceReference;

namespace UniversityExamsExcelAddIn
{
    public partial class QueryExecutionForm : Form
    {
        public QueryExecutionForm()
        {
            InitializeComponent();
        }

        private void ExecuteQueryButton_Click(object sender, EventArgs e)
        {
            if (StringValueVerificationService.IsStringValueCorrect(QueryTextBox.Text))
            {
                try
                {

                    ExecuteQueryButton.Enabled = false;
                    ServiceReference.WcfServiceClient client = new WcfServiceClient();
                    var result = client.ExecuteSelectStatement(QueryTextBox.Text);
                    if (result.Equals("Проверьте правильность введенного запроса!"))
                    {
                        MessageBox.Show("Проверьте правильность введенного запроса!", "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);

                        ExecuteQueryButton.Enabled = true;
                    }
                    else if (result.Equals("Введеннное выражение не является SELECT запросом!"))
                    {
                        MessageBox.Show("Введеннное выражение не является SELECT запросом!", "Ошибка", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        ExecuteQueryButton.Enabled = true;
                    }
                    else
                    {
                        Globals.ThisAddIn.Application.Cells.Clear();


                        String[] record = result.Split('\n');
                        for (int i = 0; i < record.Length - 1; i++)
                        {
                            String[] cellStr = record[i].Split('|');
                            for (int j = 0; j < cellStr.Length - 1; j++)
                            {
                                Globals.ThisAddIn.Application.Cells[i + 1, j + 1] = cellStr[j];
                            }
                        }
                        Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 1), GetExcelColumnName(1, record[1].Split('|').Length - 1)].Interior.Color =
                            System.Drawing.Color.DarkOrange.ToArgb();
                        Globals.ThisAddIn.Application.Range[GetExcelColumnName(1, 1), GetExcelColumnName(record.Length - 1, record[1].Split('|').Length - 1)].Columns.AutoFit();
                        Close();
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Неверный синтаксис запроса", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }


        }
        private string GetExcelColumnName(int rowNumber, int columnNumber)
        {
            int dividend = columnNumber;
            string columnName = String.Empty;
            int modulo;

            while (dividend > 0)
            {
                modulo = (dividend - 1) % 26;
                columnName = Convert.ToChar(65 + modulo).ToString() + columnName;
                dividend = (int)((dividend - modulo) / 26);
            }

            return String.Format("{0}{1}", columnName, rowNumber);
        }
    }
}

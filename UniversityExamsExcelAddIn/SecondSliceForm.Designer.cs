﻿namespace UniversityExamsExcelAddIn
{
    partial class SecondSliceForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.StartYearComboBox = new System.Windows.Forms.ComboBox();
            this.ExecuteButton = new System.Windows.Forms.Button();
            this.EndYearComboBox = new System.Windows.Forms.ComboBox();
            this.StartYearLabel = new System.Windows.Forms.Label();
            this.EndYearLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // StartYearComboBox
            // 
            this.StartYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.StartYearComboBox.FormattingEnabled = true;
            this.StartYearComboBox.Location = new System.Drawing.Point(166, 34);
            this.StartYearComboBox.Name = "StartYearComboBox";
            this.StartYearComboBox.Size = new System.Drawing.Size(357, 21);
            this.StartYearComboBox.TabIndex = 0;
            this.StartYearComboBox.SelectionChangeCommitted += new System.EventHandler(this.StartYearComboBox_SelectionChangeCommitted);
            // 
            // ExecuteButton
            // 
            this.ExecuteButton.Location = new System.Drawing.Point(403, 121);
            this.ExecuteButton.Name = "ExecuteButton";
            this.ExecuteButton.Size = new System.Drawing.Size(120, 23);
            this.ExecuteButton.TabIndex = 2;
            this.ExecuteButton.Text = "Выполнить";
            this.ExecuteButton.UseVisualStyleBackColor = true;
            this.ExecuteButton.Click += new System.EventHandler(this.ExecuteButton_Click);
            // 
            // EndYearComboBox
            // 
            this.EndYearComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EndYearComboBox.FormattingEnabled = true;
            this.EndYearComboBox.Location = new System.Drawing.Point(166, 80);
            this.EndYearComboBox.Name = "EndYearComboBox";
            this.EndYearComboBox.Size = new System.Drawing.Size(357, 21);
            this.EndYearComboBox.TabIndex = 3;
            this.EndYearComboBox.SelectionChangeCommitted += new System.EventHandler(this.EndYearComboBox_SelectionChangeCommitted);
            // 
            // StartYearLabel
            // 
            this.StartYearLabel.AutoSize = true;
            this.StartYearLabel.Location = new System.Drawing.Point(12, 37);
            this.StartYearLabel.Name = "StartYearLabel";
            this.StartYearLabel.Size = new System.Drawing.Size(89, 13);
            this.StartYearLabel.TabIndex = 4;
            this.StartYearLabel.Text = "Начало периода";
            // 
            // EndYearLabel
            // 
            this.EndYearLabel.AutoSize = true;
            this.EndYearLabel.Location = new System.Drawing.Point(12, 83);
            this.EndYearLabel.Name = "EndYearLabel";
            this.EndYearLabel.Size = new System.Drawing.Size(83, 13);
            this.EndYearLabel.TabIndex = 5;
            this.EndYearLabel.Text = "Конец периода";
            // 
            // SecondSliceForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(531, 154);
            this.Controls.Add(this.EndYearLabel);
            this.Controls.Add(this.StartYearLabel);
            this.Controls.Add(this.EndYearComboBox);
            this.Controls.Add(this.ExecuteButton);
            this.Controls.Add(this.StartYearComboBox);
            this.Name = "SecondSliceForm";
            this.Text = "Динамика успеваемости в зависимости от ученого звания преподавателя";
            this.Load += new System.EventHandler(this.SecondSliceForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox StartYearComboBox;
        private System.Windows.Forms.Button ExecuteButton;
        private System.Windows.Forms.ComboBox EndYearComboBox;
        private System.Windows.Forms.Label StartYearLabel;
        private System.Windows.Forms.Label EndYearLabel;
    }
}
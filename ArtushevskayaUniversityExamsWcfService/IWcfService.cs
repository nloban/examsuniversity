﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ArtushevskayaUniversityExamsWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWcfService" in both code and config file together.
    [ServiceContract]
    public interface IWcfService
    {
        [OperationContract]
        String ExecuteSelectStatement(String query);

        [OperationContract]
        void GenerateFacts(DateTime startDate, DateTime endDate, UInt32 amount);

        // TODO: Add your service operations here
    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
}

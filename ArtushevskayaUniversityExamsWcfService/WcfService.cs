﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using ArtushevskayaUniversityExamsDataAccess;

namespace ArtushevskayaUniversityExamsWcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WcfService" in both code and config file together.
    public class WcfService : IWcfService
    {

        private readonly EntitiesModel entitiesModel = EntitiesModel.Current; 

        public String ExecuteSelectStatement(String query)
        {
            String result = String.Empty;
            if (query.Split(' ')[0].ToUpper() == "SELECT")
            {
                try
                {
                    result = "";
                    using (SqlConnection connection =
                        new SqlConnection((ConfigurationManager.ConnectionStrings["ArtushevskayaUniversityExamsDBConnection"]).ConnectionString)
                        )
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        command.CommandType = CommandType.Text;
                        command.CommandTimeout = 0;
                        using (IDataReader rdr = command.ExecuteReader())
                        {
                            for (int i = 0; i < rdr.FieldCount; i++)
                            {
                                result += rdr.GetName(i) + "|";
                            }
                            result += "\n";
                            while (rdr.Read())
                            {
                                for (int i = 0; i < rdr.FieldCount; i++)
                                {
                                    result += rdr[i] + "|";
                                }
                                result += "\n";
                            }
                        }
                    }
                }
                catch (Exception exception)
                {
                    result = "Проверьте правильность введенного запроса!";
                }
            }
            else
            {
                result = "Введеннное выражение не является SELECT запросом!";
            }

            return result;
        }

        public void GenerateFacts(DateTime startDate, DateTime endDate, UInt32 amount)
        {
            Random random = new Random();
            var teachers = new HashSet<Teacher>(entitiesModel.Teachers);
            var teachersCount = teachers.Count;
            var students = new HashSet<Student>(entitiesModel.Students);
            var studentsCount = students.Count;
            var subjects = new HashSet<Subject>(entitiesModel.Subjects);
            var subjectsCount = subjects.Count;
            var marks = new Int32[] {3, 4, 5, 6, 7, 8, 9, 10};
            var semestrs = new Int32[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
            var dayDifference = (endDate - startDate).Days;

            using (SqlConnection connection =
                new SqlConnection(
                    (ConfigurationManager.ConnectionStrings["ArtushevskayaUniversityExamsDBConnection"]).ConnectionString))
            {
                connection.Open();
                for (int i = 0; i < amount; i++)
                {
                    var date = startDate.AddDays(random.Next(dayDifference));
                    AddDateDimensionToDatabase(date, connection);

                    var fact = new UniversityExamsFact()
                    {
                        DateID = date,
                        Mark = marks.ElementAt(random.Next(marks.Count())),
                        Semester = semestrs.ElementAt(random.Next(semestrs.Count())),
                        Student = students.ElementAt(random.Next(studentsCount)),
                        Teacher = teachers.ElementAt(random.Next(teachersCount)),
                        Subject = subjects.ElementAt(random.Next(subjectsCount))
                    };

                    try
                    {
                        AddFactToDatabase(fact, connection);
                    }
                    catch (Exception)
                    {
                    }

                }
            }
        }

        private void AddDateDimensionToDatabase(DateTime dateTime, SqlConnection connection)
        {
            try
            {
                new SqlCommand("Insert into DateDimension (DateId) values('" + dateTime + "')",
                    connection).ExecuteNonQuery();
            }
            catch (SqlException exception)
            {
            }
        }

        private void AddFactToDatabase(UniversityExamsFact fact, SqlConnection connection)
        {
            String command = String.Format("{0} '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                @"Insert into UniversityExamsFact (TeacherID, SubjectID, StudentID, Semester, Mark, DateID) values(",
                fact.Teacher.TeacherID, fact.Subject.SubjectID, fact.Student.StudentID,
                fact.Semester, fact.Mark, fact.DateID);
            new SqlCommand(command, connection).ExecuteNonQuery();
        }
    }
}
